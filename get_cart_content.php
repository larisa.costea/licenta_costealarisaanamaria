<?php
header("Access-Control-Allow-Origin: *"); // Acest header permite accesul CORS
header("Content-Type: application/json; charset=UTF-8");

$host = 'localhost';
$db   = 'licenta';
$user = 'root';
$pass = '';
$charset = 'utf8mb4';

$dsn = "mysql:host=$host;dbname=$db;charset=$charset";
$opt = [
    PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
    PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
    PDO::ATTR_EMULATE_PREPARES   => false,
];
$pdo = new PDO($dsn, $user, $pass, $opt);

$stmt = $pdo->query('SELECT * FROM produse');
$data = $stmt->fetchAll();

echo json_encode($data);
?>
