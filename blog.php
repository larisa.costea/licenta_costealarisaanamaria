<?php
session_start();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Happy Paws</title>
    <!-- font awesome link-->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css">
    <!-- swiper css link -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/swiper@9/swiper-bundle.min.css" />
    <link rel="stylesheet" href="style.css">
</head>
<body>
    <header class="header">
        <a href="index.php" class="logo"> <i class="fa-solid fa-paw"></i>Happy Paws</a>
        <nav class="navbar">
            <a href="index.php">Acasa</a>
            <a href="about.php">Despre Noi</a>
            <a href="preturi.php">Servicii/Tarife</a>
            <a href="cont.php">Contul Meu</a>
            <a href="contactCOD.php">Contact</a>
            <a class="active" href="blog.php">Blog</a>
            <a href="test.php">Produse</a>
            <?php
if (isset($_SESSION["isAdmin"]) && $_SESSION["isAdmin"] == true) {
    echo '<a href="admin.php">Panou Admin</a>';
}
?>
        </nav>
        <div class="icons">
            <div id="login-btn" class="fas fa-user"></div>
            <div id="menu-btn" class="fas fa-bars"></div>
            
        </div>
        <!-- login form-->
        <?php
if (isset($_SESSION["isLogged"]) && $_SESSION["isLogged"] == true) {
    // Utilizatorul este autentificat
    echo '<a href="logout.php" class="btn btn-logout">Logout</a>';
} else {
    // Utilizatorul nu este autentificat
?>
        <form action="login.php" method="POST" class="login-form">
            <h3>login form <i class="fa-solid fa-paw"></i></h3>
            <input type="email" name="email" placeholder="Introduceti adresa de email" class="box">
            <input type="password" name="parola" placeholder="Introduceti parola" class="box">
            <div class="remember">
                <input type="checkbox" name="remember" id="remember-me">
                <label for="remember-me"> remember me</label>
            </div>
            <label for="rol">Selectează rolul:</label>
            <select name="rol" id="rol">
                <option value="user">Utilizator</option>
                <option value="admin">Admin</option>
            </select>
            <button type="submit" class="btn">login</button>
        </form>
        <?php
}
?>
    </header>
<!--header section ends -->


<!--home section starts-->
<section class="homeBlog" id="homeBlog">
    <div class="content" >
        <h3> Vezi lumea prin <span> ochii animalelor <br> </span> </h3>
        <p>- blogul dedicat sănătății și bunăstării lor!</p>
       
    </div>
    
    <div class="custom-shape-divider-bottom-1684086027">
        <svg data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1200 120" preserveAspectRatio="none">
            <path d="M321.39,56.44c58-10.79,114.16-30.13,172-41.86,82.39-16.72,168.19-17.73,250.45-.39C823.78,31,906.67,72,985.66,92.83c70.05,18.48,146.53,26.09,214.34,3V0H0V27.35A600.21,600.21,0,0,0,321.39,56.44Z" class="shape-fill"></path>
        </svg>
    </div>
     </section>
<!-- home section ends-->
    <main>
        <div class="centerTitlu">
            <h1>Articole de Blog</h1>
        </div>
        <div class="search-container">
            <input type="text" id="search-input" placeholder="Caută...">
            <button id="search-button">Caută</button>
            <button id="reset-button" style="display: none;">Revenire</button>
        </div>
        <div id="article-container">
            <div id="no-results-message" style="display: none; text-align: center; margin-top: 20px;text-transform: none;">
                Nu există articole cu acest conținut.
            </div>
            
            <article>
                <h2><a href="rase.html ">Top 10 rase de câini potrivite pentru familii</a></h2>
                <p>Data publicării: 25 Mai 2023</p>
                <img src="image/rase.jpg" alt="rase">
                <p>O listă a celor mai potrivite rase de câini pentru familii, inclusiv informații despre personalitatea, dimensiunea și nevoile de îngrijire ale fiecărei rase.</p>
                <h3>Comentarii:</h3>
                <p><strong>John:</strong> Ce cățeluș adorabil!</p>
                <p><strong>Mary:</strong> Îmi place mult!</p>
            </article>
            <article>
                <h2><a href="pregatire_casa.html">Cum să îți pregătești casa pentru un nou cățeluș</a></h2>
                <p>Data publicării: 26 Mai 2023</p>
                <img src="image/casa.jpg" alt="casa">
                <p>Un ghid pas cu pas pentru pregătirea casei pentru sosirea unui cățeluș, inclusiv achiziționarea de accesorii necesare, pregătirea unui loc de odihnă și asigurarea unui mediu sigur.</p>
                <h3>Comentarii:</h3>
                <p><strong>Susan:</strong> Ce pisică drăguță!</p>
                <p><strong>Mike:</strong> Pisica ta seamănă cu a mea!</p>
            </article>
            <article>
                <h2><a href="antrenament.html">Sfaturi pentru antrenarea cățelușului</a></h2>
                <p>Data publicării: 26 Mai 2023</p>
                <img src="image/antrenament.jpg" alt="antrenament">
                <p>Ghid pentru începerea procesului de antrenare a cățelușului, inclusiv comenzi de bază, gestionarea comportamentelor nedorite și metode eficiente de recompensare.</p>
                <h3>Comentarii:</h3>
                <p><strong>Susan:</strong> Ce pisică drăguță!</p>
                <p><strong>Mike:</strong> Pisica ta seamănă cu a mea!</p>
            </article>
            <article >
                <h2><a href="hrana.html">Sfaturi pentru alegerea hranei potrivite pentru pisică</a></h2>
                <p>Data publicării: 26 Mai 2023</p>
                <img src="image/food.jpg" alt="food">
                <p>Informații despre necesitățile nutriționale ale pisicilor și ghid pentru alegerea hranei adecvate, inclusiv informații despre ingrediente, tipuri de alimente și frecvența hranirii</p>
                <h3>Comentarii:</h3>
                <p><strong>Susan:</strong> Ce pisică drăguță!</p>
                <p><strong>Mike:</strong> Pisica ta seamănă cu a mea!</p>
            </article>
            <article>
                <h2><a href="stres.html">Sfaturi pentru gestionarea stresului la animalele de companie</a></h2>
                <p>Data publicării: 26 Mai 2023</p>
                <img src="image/stress.jpg" alt="Lazy cat">
                <p>Ghid pentru începerea procesului de antrenare a cățelușului, inclusiv comenzi de bază, gestionarea comportamentelor nedorite și metode eficiente de recompensare.</p>
                <h3>Comentarii:</h3>
                <p><strong>Susan:</strong> Ce pisică drăguță!</p>
                <p><strong>Mike:</strong> Pisica ta seamănă cu a mea!</p>
            </article>
            <article>
                <h2><a href="blana.html">Sfaturi pentru îngrijirea și curățarea blănii animalelor de companie</h2>
                <p>Data publicării: 26 Mai 2023</p>
                <img src="image/blana.jpg" alt="blana">
                <p> Ghid detaliat pentru a menține blana animalului tău de companie curată și sănătoasă, inclusiv tehnici de periere și băi corecte.</p>
                <h3>Comentarii:</h3>
                <p><strong>Susan:</strong> Ce pisică drăguță!</p>
                <p><strong>Mike:</strong> Pisica ta seamănă cu a mea!</p>
            </article>
        </div>
    </main>
    <script src="script.js"></script>
</body>
</html>


<script>
  document.getElementById('search-button').addEventListener('click', function() {
    var searchInput = document.getElementById('search-input').value.toLowerCase();
    var articles = document.getElementsByTagName('article');
    var noResultsMessage = document.getElementById('no-results-message');
    var resetButton = document.getElementById('reset-button');

    var foundArticles = 0;

    for (var i = 0; i < articles.length; i++) {
        var article = articles[i];
        var articleTitle = article.getElementsByTagName('h2')[0].textContent.toLowerCase();
        var articleContent = article.getElementsByTagName('p')[1].textContent.toLowerCase();
        
        if (articleTitle.includes(searchInput) || articleContent.includes(searchInput)) {
            article.style.display = 'block';
            foundArticles++;
        } else {
            article.style.display = 'none';
        }
    }

    if (foundArticles === 0) {
        noResultsMessage.style.display = 'block';
        resetButton.style.display = 'inline-block';
    } else {
        noResultsMessage.style.display = 'none';
        resetButton.style.display = 'inline-block';
    }
});

document.getElementById('reset-button').addEventListener('click', function() {
    var articles = document.getElementsByTagName('article');
    var noResultsMessage = document.getElementById('no-results-message');
    var resetButton = document.getElementById('reset-button');

    for (var i = 0; i < articles.length; i++) {
        var article = articles[i];
        article.style.display = 'block';
    }

    noResultsMessage.style.display = 'none';
    resetButton.style.display = 'none';
});




</script>