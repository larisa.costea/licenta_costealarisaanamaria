<?php
// Conectarea la baza de date
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "licenta";

$conn = mysqli_connect($servername, $username, $password, $dbname);
if (!$conn) {
    die("Conexiune esuata: " . mysqli_connect_error());
}

// ID-ul produsului pe care doriți să-l afișați
$productID = 1;

// Interogarea pentru a extrage informațiile despre produs din tabela "Produse"
$sql = "SELECT * FROM Produse WHERE ProductID = $productID";
$result = mysqli_query($conn, $sql);

if (mysqli_num_rows($result) > 0) {
    // Afisarea produsului
    while ($row = mysqli_fetch_assoc($result)) {
        $productName = $row["ProductName"];
        $description = $row["Description"];
        $price = $row["Price"];
        $category = $row["Category"];
        $quantity = $row["Quantity"];
        $quantitySold = $row["QuantitySold"];

        // Afisarea informatiilor despre produs in codul HTML
        echo "<div class='product-card'>";
        echo "<h2>$productName</h2>";
        echo "<p>$description</p>";
        echo "<p>Pret: $price LEI</p>";
        // Afisati alte informatii despre produs, cum ar fi imaginea, cantitatea etc.
        echo "</div>";
    }
} else {
    echo "Nu s-a gasit niciun produs cu ID-ul specificat.";
}

// Închiderea conexiunii
mysqli_close($conn);
?>
