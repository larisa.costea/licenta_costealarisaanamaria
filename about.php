<?php
session_start();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Happy Paws</title>
    <!-- font awesome link-->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css">
    <!-- swiper css link -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/swiper@9/swiper-bundle.min.css" />
    <!-- custom css link -->
    <link rel="stylesheet" href="style.css">
   
    


</head>
<body>
    <header class="header">
        <a href="index.php" class="logo"> <i class="fa-solid fa-paw"></i>Happy Paws</a>
        <nav class="navbar">
            <a href="index.php">Acasa</a>
            <a class="active" href="about.php">Despre Noi</a>
            <a href="preturi.php">Servicii/Tarife</a>
            <a href="cont.php">Contul Meu</a>
            <a href="contactCOD.php">Contact</a>
            <a href="blog.php">Blog</a>
            <a href="test.php">Produse</a>
            <?php
if (isset($_SESSION["isAdmin"]) && $_SESSION["isAdmin"] == true) {
    echo '<a href="admin.php">Panou Administrator</a>';
}
?>
        </nav>
        <div class="icons">
            <div id="login-btn" class="fas fa-user"></div>
            <div id="menu-btn" class="fas fa-bars"></div>
            
        </div>
        <!-- login form-->
        <?php
if (isset($_SESSION["isLogged"]) && $_SESSION["isLogged"] == true) {
    // Utilizatorul este autentificat
    echo '<a href="logout.php" class="btn btn-logout">Logout</a>';
} else {
    // Utilizatorul nu este autentificat
?>
        <form action="login.php" method="POST" class="login-form">
            <h3>login form <i class="fa-solid fa-paw"></i></h3>
            <input type="email" name="email" placeholder="Introduceti adresa de email" class="box">
            <input type="password" name="parola" placeholder="Introduceti parola" class="box">
            <div class="remember">
                <input type="checkbox" name="remember" id="remember-me">
                <label for="remember-me"> remember me</label>
            </div>
            <label for="rol">Selectează rolul:</label>
            <select name="rol" id="rol">
                <option value="user">Utilizator</option>
                <option value="admin">Admin</option>
            </select>
            <button type="submit" class="btn">login</button>
        </form>
        <?php
}
?>
    </header>
<!--header section ends -->

<!--home section starts-->
<section class="aboutp" id="aboutp">
    <div class="content">
        <h3> Despre noi </h3>
        <p>Trăim într-o lume unde zâmbetele animalelor ne sunt ghid.<br> Află mai multe!</p>
        
    </div>
    
    <div class="custom-shape-divider-bottom-1684086027">
        <svg data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1200 120" preserveAspectRatio="none">
            <path d="M321.39,56.44c58-10.79,114.16-30.13,172-41.86,82.39-16.72,168.19-17.73,250.45-.39C823.78,31,906.67,72,985.66,92.83c70.05,18.48,146.53,26.09,214.34,3V0H0V27.35A600.21,600.21,0,0,0,321.39,56.44Z" class="shape-fill"></path>
        </svg>
    </div>
     </section>
<!-- home section ends-->
<!-- about -->

<section class="about" id="about">

    <h2 class="deco-title">Despre noi</h2>

    <div class="box-container">

        <div class="image">
            <img src="image/cute.png" alt="">
        </div>

        <div class="content">
            <h3 class="title">De ce HappyPaws?</h3>
            <p>Alege HappyPaws pentru că suntem mai mult decât un simplu cabinet veterinar.
                 Suntem o echipă pasionată și dedicată de medici
                  veterinari care pun binele și sănătatea animalelor pe primul loc</p>
            
            <div class="icons-container">
                <div class="icons">
                    <h3><strong>Expertiza și Competența</strong><i class="fas fa-certificate"></i> </h3>
                    
                </div>
                <div class="icons">
                    <i class="fas fa-heart"></i>
                    <h3><strong>Abordare Personalizată și Empatică</strong></h3>
                </div>
                <div class="icons">
                    <i class="fas fa-tools"></i>
                    <h3><strong>Servicii Complete</strong></h3>
                </div>
                <a href="contactCOD.php" class="btn">Programare</a>
            </div>
        </div>

    

</section>

<!--Description section starts-->
<div class="description">
    <div class="box-container">
        <div class="left-column">
            <div class="accordion">
                <div class="accordion-item">
                  <div class="accordion-header">
                    <i class="fas fa-chevron-down"></i>
                    <h3>Servicii Medicale</h3>
                    <button class="accordion-toggle">+</button>
                  </div>
                  <div class="accordion-content">
                    <p>Oferim o gamă completă de servicii medicale veterinare,
                       inclusiv consultații generale, vaccinări, analize de laborator, 
                       imagistică și tratamente medicale. Echipa noastră de medici veterinari
                        cu experiență este dedicată să ofere cele mai bune îngrijiri
                         și să mențină sănătatea animalelor de companie.</p>
                  </div>
                </div>
                <div class="accordion-item">
                    <div class="accordion-header">
                      <i class="fas fa-chevron-down"></i>
                      <h3>Intervenții chirurgicale</h3>
                      <button class="accordion-toggle">+</button>
                    </div>
                    <div class="accordion-content">
                      <p> Efectuăm intervenții chirurgicale de înaltă calitate pentru animalele de companie,
                         inclusiv sterilizare/castrare, îndepărtare de tumori, chirurgie ortopedică și multe altele.
                          Echipa noastră de chirurgi veterinari este bine pregătită și utilizează
                           echipamente moderne pentru a asigura proceduri sigure și eficiente.</p>
                    </div>
                  </div>
                  <div class="accordion-item">
                    <div class="accordion-header">
                      <i class="fas fa-chevron-down"></i>
                      <h3><strong> Urgențe Veterinare</strong></h3>
                      <button class="accordion-toggle">+</button>
                    </div>
                    <div class="accordion-content">
                      <p>Suntem disponibili 24/7 pentru a oferi asistență în caz de urgențe veterinare.
                        Indiferent de problema cu care se confruntă animalul dvs., 
                        vom răspunde prompt și vom oferi îngrijiri de urgență de înaltă calitate.
                         Echipa noastră este pregătită să gestioneze diverse situații de urgență,
                          de la traumatisme și intoxicații la probleme respiratorii sau cardiace grave.</p>
                    </div>
                  </div>
                  <div class="accordion-item">
                    <div class="accordion-header">
                      <i class="fas fa-chevron-down"></i>
                      <h3>Consultanță nutrițională</h3>
                      <button class="accordion-toggle">+</button>
                    </div>
                    <div class="accordion-content">
                      <p>Oferim servicii de consultanță nutrițională specializată pentru animalele de companie.
                         Vom lucra împreună cu dvs. pentru a crea un plan alimentar adecvat nevoilor
                          și stării de sănătate a animalului dvs. 
                          Vom oferi sfaturi cu privire la hrana potrivită, cantitatea și frecvența hrănirii, 
                          precum și soluții pentru problemele legate de alimentație.</p>
                    </div>
                  </div>
              </div>
            </div> 
                  
            <div class="right-column">
        <div class="container">  
            <div class="progress-label">Servicii medicale</div>  
            <div class="progress progress-moved"> 
              <div class="progress-bar" > </div> </div>
              <div class="progress-label2">Intervenții chirurgicale</div>
              <div class="progress2 progress-moved">
              <div class="progress-bar2" > </div>  </div>   
              <div class="progress-label3">Urgențe veterinare</div>
              <div class="progress3 progress-moved">
              <div class="progress-bar3" > </div>  </div>   
              <div class="progress-label4">Consultanță nutrițională</div>
              <div class="progress4 progress-moved">
              <div class="progress-bar4" > </div> </div> 
          </div>
            </div> 
    </div>
        
    </div>
    <!--Description section ends-->

    <!--Cards section starts-->
    <section class="page-contain" id="page-contain">
      <div class="card-container">
      <div class="card">
        <div class="card-bg">
          <img src="image/inu.jpg" alt="Imagine de fundal">
  </div>
        <div class="card-content">
          <span class="card-icon">
            <i class="fas fa-map-marker-alt fa-lg"></i>
          </span>
          <h3 class="card-title">Locație</h3>
          <hr class="card-divider">
          <p class="card-description">Timișoara, Piața Victoriei</p>
          <a href="contactCOD.php" class="card-button">Detalii &rarr;</a>
        </div>
      </div>

      <div class="card">
        <div class="card-bg">
          <img src="image/location.jpg" alt="Imagine de fundal">
  </div>
        <div class="card-content">
          <span class="card-icon">
            <i class="fas fa-envelope fa-lg"></i>
          </span>
          <h3 class="card-title">Contact</h3>
          <hr class="card-divider">
          <p class="card-description">Telefon: 077X XXX XXX <br> Email: contact@happypaws.com</p>
          <a href="contactCOD.php" class="card-button">Detalii &rarr;</a>
        </div>
      </div>

      <div class="card">
        <div class="card-bg">
          <img src="image/program.jpg" alt="Imagine de fundal">
  </div>
        <div class="card-content">
          <span class="card-icon">
            <i class="fas fa-clock fa-lg"></i>
          </span>
          <h3 class="card-title">Program</h3>
          <hr class="card-divider">
          <p class="card-description">Luni-Vineri: 09:00-18:00 <br> Sambata: 09:00-16:00 <br> Duminica: Inchis</p>
          <a href="contactCOD.php" class="card-button">Detalii &rarr;</a>
        </div>
      </div>
    </div>
    </section>
    <!--Cards section ends-->
    <!--team section starts-->
    <section class="team-section" id="team-section">
      <h2 class="section-title">Echipa noastră</h2>
      <div class="member-line"></div>
      <div class="team-member">
        <div class="member-image">
          <img src="image/doctor.png" alt="Membre echipa">
        </div>
        <div class="member-details">
          <h3 class="member-name">John Doe</h3>
          <p class="member-role">Medic Veterinar</p>
        </div>
      </div>
      
      <div class="team-member">
        <div class="member-image">
          <img src="image/doctor.png" alt="Membre echipa">
        </div>
        <div class="member-details">
          <h3 class="member-name">Jane Smith</h3>
          <p class="member-role">Medic Veterinar</p>
        </div>
      </div>
      
      <div class="team-member">
        <div class="member-image">
          <img src="image/doctor.png" alt="Membre echipa">
        </div>
        <div class="member-details">
          <h3 class="member-name">Jane Smith</h3>
          <p class="member-role">Medic Veterinar</p>
        </div>
      </div>
      
    </section>
    <!--teams section ends-->
    <section class="testimonial-section">
      <h2 class="section-title">Testimoniale</h2>
      <div class="testimonial-container">
        
        <div class="testimonial-card">
          <img src="image/hh.jpg" alt="Poza testimonial 1">
          <h3>John Doe</h3>
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
        </div>
        <div class="testimonial-card">
          <img src="image/hh.jpg" alt="Poza testimonial 2">
          <h3>Jane Smith</h3>
          <p>Integer vitae magna nec massa consequat consequat.</p>
        </div>
        <div class="testimonial-card">
          <img src="image/hh.jpg" alt="Poza testimonial 3">
          <h3>David Johnson</h3>
          <p>Sed ac nunc vitae mi cursus tincidunt.</p>
        </div>
      </div>
    </section>
  
    <section class="contact" id="contact">

      <div class="image">
          <img src="hero.png" alt="">
      </div>
  
      <form action="contact.php" method="POST">
        <h3>contact us</h3>
        <input type="text" name="name" placeholder="your name" class="box">
        <input type="email" name="email" placeholder="your email" class="box">
        <input type="tumber" name="phone" placeholder="your number" class="box">
        <textarea name="message" placeholder="your message" id="" cols="30" rows="10"></textarea>
        <input type="submit" value="send message" class="btn">
    </form>

  
  </section>
    
  <footer class="footer">
  
    <div class="footer-info">
      
      <h2>Happy Paws</h2>
      <p>Ne dedicăm să oferim cele mai bune servicii pentru a vă ajuta să vă îngrijiți de animalele dumneavoastră de companie. Ne pasă de bunăstarea lor și vrem să vă asigurăm că sunt în cele mai bune mâini.</p>
    </div>
    <div class="footer-links">
      <h3>Link-uri utile</h3>
      <ul>
        <li><a href="index.html">Acasă</a></li>
        <li><a href="about.html">Despre noi</a></li>
        <li><a href="preturi.html">Servicii/Tarife</a></li>
        <li><a href="cont.html">Contul meu</a></li>
        <li><a href="contact.html">Contact</a></li>
      </ul>
    </div>
    <div class="footer-social">
      <h3>Urmăriți-ne</h3>
      <ul>
        <li><a href="#"><i class="fab fa-facebook"></i> Facebook</a></li>
        <li><a href="#"><i class="fab fa-instagram"></i> Instagram</a></li>
        <li><a href="#"><i class="fab fa-twitter"></i> Twitter</a></li>
        <li><a href="#"><i class="fab fa-linkedin"></i> LinkedIn</a></li>
      </ul>
    </div>
    <div class="footer-contact">
      <h3>Contact</h3>
      <p><i class="fa-solid fa-envelope"></i> contact@happypaws.com</p>
      <p><i class="fa-solid fa-phone"></i> +123456789</p>
      <p><i class="fa-solid fa-map-marker-alt"></i> Str. Exemplu, Nr. 1, Oras, Judet, Romania</p>
    </div>
  </div>
  <div class="footer-bottom">
    <p>&copy; 2023 Happy Paws. Toate drepturile rezervate.</p>
  </div>
</footer>

 <!-- jquery cdn link -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.4/jquery.min.js"></script>
    <!-- swiper js link -->
    <script src="https://cdn.jsdelivr.net/npm/swiper@9/swiper-bundle.min.js"></script>
    <!-- custom js link -->
    
    <script src="script.js"></script>
    <script>
     

      </script>
</body>
</html>