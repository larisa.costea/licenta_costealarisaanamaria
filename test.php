<?php
session_start();
?>
<!DOCTYPE html>
<html lang="en">
<head>
   <meta charset="UTF-8">
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <meta name="viewport" content="width=device-width, initial-scale=1.0">
   <title>Happy Paws</title>
   <!-- font awesome link-->
   <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css">
   <!-- swiper css link -->
   <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/swiper@9/swiper-bundle.min.css" />
   <!-- custom css link -->
   <link rel="stylesheet" href="style1.css">
</head>
<body >
   <header class="header">
      <a href="index.php" class="logo"> <i class="fa-solid fa-paw"></i>Happy Paws</a>
      <nav class="navbar">
         <a href="index.php"><strong>Acasa</strong></a>
         <a href="about.php"><strong>Despre Noi</strong></a>
         <a href="preturi.php"><strong>Servicii/Tarife</strong></a>
         <a href="cont.php"><strong>Contul Meu</strong></a>
         <a href="contactCOD.php"><strong>Contact</strong></a>
         <a href="blog.php"><strong>Blog</strong></a>
         <a class="active" href="#Produse"><strong>Produse</strong></a>
         <?php
if (isset($_SESSION["isAdmin"]) && $_SESSION["isAdmin"] == true) {
    echo '<a href="admin.php">Panou Admin</a>';
}
?>
      </nav>

      <div class="icons" style="display: flex; justify-content: space-between;">
     
        
         <div id="login-btn" class="fas fa-user"></div>  
         <div class="shopping">
            <img src="image/shopping.svg">
            <span class="quantity">0</span>
         </div>
      </div>
      <?php
    if (isset($_SESSION["isLogged"]) && $_SESSION["isLogged"] == true) {
    // Utilizatorul este autentificat
    echo '<a href="logout.php" class="btn btn-logout">Logout</a>';
    } else {
    // Utilizatorul nu este autentificat
    ?>
      <!-- login form-->
    <form action="login.php" method="POST" class="login-form">
      <h3>Autentificare <i class="fa-solid fa-paw"></i></h3>
      <input type="email" name="email" placeholder="Introduceti adresa de email" class="box">
      <input type="password" name="parola" placeholder="Introduceti parola" class="box">
      
      <label for="rol">Selectează rolul:</label>
      <select name="rol" id="rol">
          <option value="user">Utilizator</option>
          <option value="admin">Admin</option>
      </select>
      <button type="submit" class="btn">Autentificare</button>
  </form>
  <?php
    }
    ?>
   </header>
   <section class="home" id="home">
      <div class="content">
        <h3> Produse</h3>
  
      </div>
  
      <div class="custom-shape-divider-bottom-1684086027">
        <svg data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1200 120"
          preserveAspectRatio="none">
          <path
            d="M321.39,56.44c58-10.79,114.16-30.13,172-41.86,82.39-16.72,168.19-17.73,250.45-.39C823.78,31,906.67,72,985.66,92.83c70.05,18.48,146.53,26.09,214.34,3V0H0V27.35A600.21,600.21,0,0,0,321.39,56.44Z"
            class="shape-fill"></path>
        </svg>
      </div>
    </section>
    <div class="search-containerM">
      <input type="text" id="searchbarM" placeholder="Caută produse...">
      <button type="submit" id="searchButton">Caută</button>
      <select id="filter">
  <option value="all">Toate produsele</option>
  <option value="Pisica">Pisica</option>
  <option value="Caini">Caini</option>
  <!-- Adaugă mai multe categorii după nevoie -->
</select>

    </div>
   <div class="container">
      <div class="list" id="product-list">
         <!-- Aici vei afișa produsele -->
         </div>
   </div>
  
   
   <div class="card">
      <h1>Coș de cumpărături</h1>
      <ul class="listCard">
      </ul>
      <div class="checkOut">
         <div class="total">Sumă totală: 0</div>
         <div class="buttons">
            <button class="closeShopping">Închide</button>
            <button id="buyButton" class="buyButton">Cumpără</button>
         </div>
      </div>
   </div>
   

   <script src="app.js"></script>

<!-- jquery cdn link -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.4/jquery.min.js"></script>
<!-- swiper js link -->
<script src="https://cdn.jsdelivr.net/npm/swiper@9/swiper-bundle.min.js"></script>
<!-- custom js link -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script>
   /*login mesaj*/
   $(document).ready(function() {
  $(".login-form").on('submit', function(e) {
    e.preventDefault();

    var form = $(this);
    var formData = form.serialize();

    $.ajax({
      type: "POST",
      url: "login.php",
      data: formData,
      success: function(response) {
        alert( response);
        location.reload(); // Reîncarcă pagina curentă
      },
      error: function() {
        alert('A apărut o eroare în timpul autentificării.');
      }
    });
  });
});
   </script>
</body>
</html>
