<?php
session_start();

// Conectare la baza de date
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "licenta";
$conn = new mysqli($servername, $username, $password, $dbname);
if ($conn->connect_error) {
    die("Conexiunea la baza de date a eșuat: " . $conn->connect_error);
}

// Procesarea formularului de autentificare
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $email = $_POST["email"];
    $parola = $_POST["parola"];
    $rol= $_POST["rol"];

    // Obținerea informațiilor despre utilizator din baza de date
    $query = "SELECT * FROM utilizatori WHERE email = '$email'";
    $result = $conn->query($query);

    if ($result->num_rows > 0) {
        $row = $result->fetch_assoc();
        $storedPassword = $row["parola"];

        // Verificarea parolei criptate
        if (password_verify($parola, $storedPassword)) {
            // Verificarea dacă utilizatorul este admin și dacă rolul selectat corespunde cu rolul utilizatorului
            if ($row["rol"] == 'admin' && $rol == 'admin') {
                $_SESSION["userID"] = $row["id"];  // setarea id-ului utilizatorului
                $_SESSION["email"] = $email;
                $_SESSION["isLogged"] = true;
                $_SESSION["isAdmin"] = true;
                echo "Autentificare reușită. Bine ai venit, Admin!";
            } elseif ($row["rol"] == 'user' && $rol == 'user') {
                $_SESSION["userID"] = $row["id"];  // setarea id-ului utilizatorului
                $_SESSION["email"] = $email;
                $_SESSION["isLogged"] = true;
                $_SESSION["isAdmin"] = false;
                echo "Autentificare reușită. Bine ai venit, Utilizator!";
            } else {
                // Rolul selectat nu corespunde cu rolul utilizatorului
                echo "Rolul selectat nu corespunde cu rolul utilizatorului. Vă rugăm să încercați din nou.";
                exit; // Termină executarea scriptului PHP aici
            }
        
            exit; // Termină executarea scriptului PHP aici
        }
         // Termină executarea scriptului PHP aici
        } else {
            // Parolă incorectă
            echo "Email sau parolă incorecte. Vă rugăm să încercați din nou.";
            exit; // Termină executarea scriptului PHP aici
        }
    } else {
        // Utilizatorul nu există
        echo "Utilizatorul nu există";
        exit; // Termină executarea scriptului PHP aici
    }


// Închiderea conexiunii la baza de date
$conn->close();
?>
