<?php
session_start();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Happy Paws</title>
    <!-- font awesome link-->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css">
    <!-- swiper css link -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/swiper@9/swiper-bundle.min.css" />
    <!-- custom css link -->
    <link rel="stylesheet" href="style.css">
   
    


</head>
<body>
  <header class="header">
    <a href="index.php" class="logo"> <i class="fa-solid fa-paw"></i>Happy Paws</a>
    <nav class="navbar">
        <a href="index.php">Acasa</a>
        <a href="about.php">Despre Noi</a>
        <a class="active" href="preturi.php">Servicii/Tarife</a>
        <a  href="cont.php">Contul Meu</a>
        <a  href="contactCOD.php">Contact</a>
        <a  href="blog.php">Blog</a>
        <a href="test.php">Produse</a>
        <?php
if (isset($_SESSION["isAdmin"]) && $_SESSION["isAdmin"] == true) {
echo '<a href="admin.php">Panou Admin</a>';
}
?>
    </nav>
    <div class="icons">
        <div id="login-btn" class="fas fa-user"></div>
        <div id="menu-btn" class="fas fa-bars"></div>
        
    </div>
    <!-- login form-->
    <?php
if (isset($_SESSION["isLogged"]) && $_SESSION["isLogged"] == true) {
// Utilizatorul este autentificat
echo '<a href="logout.php" class="btn btn-logout">Logout</a>';
} else {
// Utilizatorul nu este autentificat
?>
    <form action="login.php" method="POST" class="login-form">
        <h3>login form <i class="fa-solid fa-paw"></i></h3>
        <input type="email" name="email" placeholder="Introduceti adresa de email" class="box">
        <input type="password" name="parola" placeholder="Introduceti parola" class="box">
        <div class="remember">
            <input type="checkbox" name="remember" id="remember-me">
            <label for="remember-me"> remember me</label>
        </div>
        <label for="rol">Selectează rolul:</label>
        <select name="rol" id="rol">
            <option value="user">Utilizator</option>
            <option value="admin">Admin</option>
        </select>
        <button type="submit" class="btn">login</button>
    </form>
    <?php
}
?>
</header>
<!--header section ends -->

<!--home section starts-->
<section class="preturi" id="preturi">
    <div class="content" >
        <h3> Explorează <br> tarifele noastre! <span><br>  Sănătatea prietenului tău cu patru labe într-un  buget prietenos.</span> </h3>
        <a href="contactCOD.php" class="btn">Programare</a>
    </div>
    
    <div class="custom-shape-divider-bottom-1684086027">
        <svg data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1200 120" preserveAspectRatio="none">
            <path d="M321.39,56.44c58-10.79,114.16-30.13,172-41.86,82.39-16.72,168.19-17.73,250.45-.39C823.78,31,906.67,72,985.66,92.83c70.05,18.48,146.53,26.09,214.34,3V0H0V27.35A600.21,600.21,0,0,0,321.39,56.44Z" class="shape-fill"></path>
        </svg>
    </div>
     </section>
<!-- home section ends-->
 <!-- about -->
 <section class="pricing-section" id="Servicii/Tarife">
    
    <div class="pricing-card">
      <div class="card-header">
        <i class="fa-solid fa-stethoscope"></i>
        <h2>General</h2>
      </div>
      <ul class="service-list">
        <li>Consultatie medicina generala -> <strong>70 lei</strong> </li>
        <li>Consultatie la domiciliu -><strong>250 lei</strong> </li>
        <li>Ecografie abdominala -> <strong>50 lei</strong></li>
        <li>Eliberare reteta -> <strong>10 lei</strong></li>
        <li>Carnet de sanatate ->  <strong>10 lei</strong></li>
        <li>Microcipare - Identificare -><strong>60 lei</strong> </li>
        <li>Pasaport -><strong>100 lei</strong> </li>
        <li>Fluidoterapie, echilibrare hidroelectrolitica si acidobazica + amplasare catater IV -><strong>50-70 lei</strong> </li>
        <li>Cateterism uretral -><strong>100 lei</strong> </li>
        <li>Recoltare sange -><strong>20 lei</strong> </li>
        <li>Clisma -> <strong>60-100 lei</strong></li>
        <li>Urgente veterinare -> <strong>200 lei</strong></li>
      </ul>
      
    </div>
  
    <div class="pricing-card">
      <div class="card-header">
        <i class="fa-solid fa-microscope"></i>
        <h2>Testare rapida boli infectioase/parazitare</h2>
      </div>
      <ul class="service-list">
        <li>Test Rapid boli infecțioase Pisica(Pif), boli parazitare Pisica(Toxoplasmoza), boli infecțioase Pisica(Fiv+Felv), boli infecțioase Pisica(Fiv), boli infecțioase Pisica(Panleucopenie) -><strong>100 lei</strong> </li>
        <li>Test Rapid boli infecțioase Pisica(Idexx) Fiv+Felv, boli infecțioase Pisica(Idexx) PROBNP Marker Cardiac -> <strong>150 lei</strong></li>
        <li>Test Rapid boli infecțioase Caine (Jigodie), boli infecțioase Caine (Parvovirus) -> <strong>100 lei</strong></li>
        <li>Test Rapid boli infecțioase Caine(Idexx 4dx) Dirofilarioza, Ehrlichioza, boala Lyme și Anaplasmoza, boli infecțioase Caine(Idexx) Parvovirus -><strong>150 lei</strong> </li>
      </ul>
      
    </div>


    <div class="pricing-card">
        <div class="card-header">
          <i class="fa-solid fas fa-syringe"></i>
          <h2>Imunizare</h2>
        </div>
        <ul class="service-list">
          <li>Vaccinare caine monovalent -> <strong>50 lei</strong> </li>
          <li>Vaccinare caine bivalent -> <strong>60 lei</strong> </li>
          <li>Vaccinare caine polivalent -><strong>80 lei</strong> </li>
          <li>Vaccinare caine antirabic -><strong>30 lei</strong> </li>
          <li>Vaccinare pisica polivalent (Tricat) -><strong>70 lei</strong> </li>
          <li>Vaccinare pisica polivalent (Purevax) -><strong>80 lei</strong> </li>
          <li>Vaccinare pisica polivalent(include si Leucemia felina) -> <strong>120 lei</strong></li>
          <li>Vaccinare pisica antirabic -> <strong>30 lei</strong></li>
          <li>Vaccinare Leporid Pestorin -><strong>50 lei</strong> </li>
        </ul>
        
      </div>
    
      <div class="pricing-card">
        <div class="card-header">
          <i class="fa-solid fas fa-bug"></i>
          <h2>Deparazitari</h2>
        </div>
        <ul class="service-list">
          <li>Deparazitare interna caine -> <strong> 10-20 lei</strong></li>
          <li>Deparazitare interna pisica -><strong>10 lei</strong> </li>
          <li>Deparazitare externa caine -> <strong>39-130 lei</strong></li>
          <li>Deparazitare externa pisica -><strong>33-90 lei</strong> </li>
        </ul>
       
      </div>
    
      <div class="pricing-card">
        <div class="card-header">
          <i class="fa-solid fa-band-aid"></i>
          <h2>Plăgi</h2>
        </div>
        <ul class="service-list">
          <li>Debridare plagi –toaleta de suprafata -> <strong>50-150 lei</strong></li>
          <li>Debridare plăgi - toaleta de suprafata + sutura plaga -><strong>150-300 lei</strong> </li>
          <li>Abcese (explorare și drenaj) -><strong>100 lei</strong> </li>
        </ul>
       
      </div>
    
      <div class="pricing-card">
        <div class="card-header">
          <i class="fa-solid fas fa-paw"></i>
          <h2>Îngrijire</h2>
        </div>
        <ul class="service-list">
          <li>Toaletare urechi -><strong>30 lei</strong> </li>
          <li>Vidare glande perianale -><strong>30 lei</strong> </li>
          <li>Taiat unghii -><strong>20 lei</strong> </li>
          <li>Ajustare unghii si cioc -> <strong>30 lei</strong></li>
          <li>Tuns caini –><strong>100 lei</strong> </li>
        </ul>
        
      </div>
    
      <div class="pricing-card">
        <div class="card-header">
          <i class="fa-solid fas fa-vial"></i>
          <h2>Testare anticorpi</h2>
        </div>
        <ul class="service-list">
          <li>CPV Ab(Parvovirus) –> <strong>70 lei</strong> </li>
          <li>CDV Ab(Distemper) –><strong>70 lei</strong> </li>
          <li>FHV Ab(Herpesvirus) –><strong>70 lei</strong> </li>
          <li>FPV Ab (Panleucopenie)-> <strong> 70 lei</strong> </li>
          <li>FCV Ab(Calicivirus) –><strong>70 lei</strong> </li>
        </ul>
        
      </div>
    
      <div class="pricing-card">
        <div class="card-header">
          <i class="fa-solid fa-tooth"></i>
          <h2>Stomatologie</h2>
        </div>
        <ul class="service-list">
          <li>Extracții (molari,premolari,canini,incisivi) -><strong>100-150 lei</strong> </li>
          <li>Extracția tuturor dinților – pisică -><strong>200 lei</strong>  </li>
          <li>Extracția tuturor dinților – câine -><strong>300 lei</strong>  </li>
          <li>Detartraj US pisica -> <strong>150 lei</strong> </li>
          <li>Detartraj US caine -><strong>150-200 lei</strong>  </li>
        </ul>
       
      </div>

      <div class="pricing-card">
        <div class="card-header">
          <i class="fa-solid fas fa-medkit"></i>
          <h2>Interventii chirurgicale</h2>
        </div>
        <ul class="service-list">
          <li>Hematom auricular-drenare si aplicare tampon steril -> <strong>150-300 lei</strong></li>
          <li>Extractie corp strain podal -><strong>100-200 lei</strong> </li>
          <li>Excizie tumoră de pleoapă -><strong>150 lei</strong> </li>
          <li>Amputare coadă -><strong>200-250 lei</strong> </li>
          <li>Remedieri fracturi mandibulare pisica -><strong> 200-250 lei</strong></li>
          <li>Hernie ombilicală -> <strong>200-350 lei</strong></li>
          <li>Castrare pisică(ovariectomie) -><strong>200 lei</strong> </li>
          <li>Castrare pisică(ovariohisterectomie) -> <strong>250 lei</strong></li>
          <li>Castrare mascul câine -><strong>300-500 lei</strong></li>
          <li>Castrare mascul caine criptorhid -><strong>350-600 lei</strong> </li>
          <li>Castrare motan -> <strong>150 lei</strong></li>
          <li>Castrare motan criptorhid -><strong>180-200 lei</strong> </li>
        </ul>
        <a class="button-text" href="pagina-programari.html">Apasă aici</a>



      </div>
      <div class="pricing-card">
        <div class="card-header">
          <i class="fa-solid fas fa-microscope"></i>
          <h2>Investigatii paraclinice</h2>
        </div>
        <ul class="service-list">
          <li>Raclaj, frotiu de sange -> <strong>30 lei</strong>  </li>
          <li>Hemoleucograma, T4, cultura si antibiograma, exsudat faringian -> <strong>100 lei</strong>  </li>
          <li>Biochimie -><strong>250 lei </strong>  </li>
          <li>Profil diagnostic complet (Hemoleucograma + Biochimie 12 parametri) -><strong>350 lei</strong>  </li>
          <li>Profil analize preoperatorii (Hemoleucograma + biochimie 5 parametri) -> <strong>250 lei</strong> </li>
          <li>Parametri individuali -> <strong>50 lei</strong> </li>
          <li>Determinare grupa sanguina caine, pisica -><strong>175 lei</strong>  </li>
          <li>Examen coproparazitologic -> <strong>50 lei</strong></li>
        </ul>
       
      </div>
    
    <!-- Adăugați mai multe carduri de prețuri aici -->
  
  </section>
  
 


<!-- end -->

<!--contact section starts-->
<section class="contact" id="contact">

    <div class="image">
        <img src="hero.png" alt="">
    </div>

    <form action="contact.php" method="POST">
      <h3>contact us</h3>
      <input type="text" name="name" placeholder="your name" class="box">
      <input type="email" name="email" placeholder="your email" class="box">
      <input type="tumber" name="phone" placeholder="your number" class="box">
      <textarea name="message" placeholder="your message" id="" cols="30" rows="10"></textarea>
      <input type="submit" value="send message" class="btn">
  </form>


</section>
<footer class="footer">
  
  <div class="footer-info">
    
    <h2>Happy Paws</h2>
    <p>Ne dedicăm să oferim cele mai bune servicii pentru a vă ajuta să vă îngrijiți de animalele dumneavoastră de companie. Ne pasă de bunăstarea lor și vrem să vă asigurăm că sunt în cele mai bune mâini.</p>
  </div>
  <div class="footer-links">
    <h3>Link-uri utile</h3>
    <ul>
      <li><a href="index.html">Acasă</a></li>
      <li><a href="about.html">Despre noi</a></li>
      <li><a href="preturi.html">Servicii/Tarife</a></li>
      <li><a href="cont.html">Contul meu</a></li>
      <li><a href="contact.html">Contact</a></li>
    </ul>
  </div>
  <div class="footer-social">
    <h3>Urmăriți-ne</h3>
    <ul>
      <li><a href="#"><i class="fab fa-facebook"></i> Facebook</a></li>
      <li><a href="#"><i class="fab fa-instagram"></i> Instagram</a></li>
      <li><a href="#"><i class="fab fa-twitter"></i> Twitter</a></li>
      <li><a href="#"><i class="fab fa-linkedin"></i> LinkedIn</a></li>
    </ul>
  </div>
  <div class="footer-contact">
    <h3>Contact</h3>
    <p><i class="fa-solid fa-envelope"></i>contact@happypaws.com</p>
    <p><i class="fa-solid fa-phone"></i> +123456789</p>
    <p><i class="fa-solid fa-map-marker-alt"></i> Str. Exemplu, Nr. 1, Oras, Judet, Romania</p>
  </div>
</div>
<div class="footer-bottom">
  <p>&copy; 2023 Happy Paws. Toate drepturile rezervate.</p>
</div>
</footer>

 <!-- jquery cdn link -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.4/jquery.min.js"></script>
    <!-- swiper js link -->
    <script src="https://cdn.jsdelivr.net/npm/swiper@9/swiper-bundle.min.js"></script>
    <!-- custom js link -->
    <script src="script.js"></script>
</body>
</html>

