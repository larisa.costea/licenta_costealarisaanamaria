<?php
session_start();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Happy Paws</title>
    <!-- font awesome link-->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css">
    <!-- swiper css link -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/swiper@9/swiper-bundle.min.css" />
    <!-- custom css link -->
    <link rel="stylesheet" href="style.css">
   
    
</head>

<body>
    <header class="header">
        <a href="index.php" class="logo"> <i class="fa-solid fa-paw"></i>Happy Paws</a>
        <nav class="navbar">
            <a href="index.php">Acasa</a>
            <a href="about.php">Despre Noi</a>
            <a href="preturi.php">Servicii/Tarife</a>
            <a  href="cont.php">Contul Meu</a>
            <a class="active" href="contactCOD.php">Contact</a>
            <a  href="blog.php">Blog</a>
            <a href="test.php">Produse</a>
            <?php
    if (isset($_SESSION["isAdmin"]) && $_SESSION["isAdmin"] == true) {
    echo '<a href="admin.php">Panou Admin</a>';
    }
    ?>
        </nav>
        <div class="icons">
            <div id="login-btn" class="fas fa-user"></div>
            <div id="menu-btn" class="fas fa-bars"></div>
            
        </div>
        <!-- login form-->
        <?php
    if (isset($_SESSION["isLogged"]) && $_SESSION["isLogged"] == true) {
    // Utilizatorul este autentificat
    echo '<a href="logout.php" class="btn btn-logout">Logout</a>';
    } else {
    // Utilizatorul nu este autentificat
    ?>
        <form action="login.php" method="POST" class="login-form">
            <h3>login form <i class="fa-solid fa-paw"></i></h3>
            <input type="email" name="email" placeholder="Introduceti adresa de email" class="box">
            <input type="password" name="parola" placeholder="Introduceti parola" class="box">
            <div class="remember">
                <input type="checkbox" name="remember" id="remember-me">
                <label for="remember-me"> remember me</label>
            </div>
            <label for="rol">Selectează rolul:</label>
            <select name="rol" id="rol">
                <option value="user">Utilizator</option>
                <option value="admin">Admin</option>
            </select>
            <button type="submit" class="btn">login</button>
        </form>
        <?php
    }
    ?>
    </header>
    <!--header section ends -->
    

<!--home section starts-->
<section class="homeC" id="homeC">
    <div class="content" >
        <h3> Contactați-ne <br><span><br> Fa o programare online</span> </h3>
        <p>Echipa HappyPaws - Pentru un zâmbet și o coadă fericită!</p>
        
    </div>
    
    <div class="custom-shape-divider-bottom-1684086027">
        <svg data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1200 120" preserveAspectRatio="none">
            <path d="M321.39,56.44c58-10.79,114.16-30.13,172-41.86,82.39-16.72,168.19-17.73,250.45-.39C823.78,31,906.67,72,985.66,92.83c70.05,18.48,146.53,26.09,214.34,3V0H0V27.35A600.21,600.21,0,0,0,321.39,56.44Z" class="shape-fill"></path>
        </svg>
    </div>
     </section>
<!-- home section ends-->
<!--contact section starts-->
<div class="middle-section">
    <div class="info-section">
        <div class="info-block">
            <i class="fas fa-map-marker-alt"></i>
            <div class="info-content">
                <h2>Locatie</h2>
                <p>Timișoara, Piața Victorie</p>
            </div>
        </div>

        <div class="info-block">
            <i class="fas fa-clock"></i>
            <div class="info-content">
                <h2>Orar</h2>
                <p>Luni - Vineri: 09:00-18:00</p>
                <p>Sambata: 09:00-16:00</p>
                <p>Duminica: Inchis</p>
            </div>
        </div>

        <div class="info-block">
            <i class="fas fa-mobile-alt"></i>
            <div class="info-content">
                <h2>Contact</h2>
                <p>Telefon: 077X XXX XXX <br>
                    Email: Contact@happypaws.Com</p>
            </div>
        </div>

        <div class="info-block">
            <i class="fab fa-whatsapp"></i>
            <div class="info-content">
                <h2>Urgente</h2>
                <p>Telefon/WhatsApp: 076X XXX XXX</p>
                
            </div>
        </div>
    </div>

<form id="booking-form" action="programare.php" method="POST">
    <h1>Programare Cabinet Veterinar</h1>
    
    <label for="name">Nume:</label>
    <input type="text" id="name" name="name" required>
    
    <label for="phone">Număr de telefon:</label>
    <input type="tel" id="phone" name="phone" required>
    
    <label for="email">Adresă Email:</label>
    <input type="email" id="email" name="email" required>
    
    <label for="animal">Tipul animalului:</label>


<select id="animal" name="animal" onchange="checkOther(this.value)">
  <option value="caine">--Selectati--</option>
  <option value="caine">Caine</option>
  <option value="pisica">Pisica</option>
  <option value="altele">Altele</option>
</select>
<input type="text" id="otherAnimal" name="otherAnimal" placeholder="Specificati tipul animalului" disabled>

    
    <label for="date">Data programării:</label>
    <input type="date" id="date" name="date" required>

    <label for="time">Ora programării (format 24 ore HH:MM):</label>
    <input type="time" id="time" name="time" pattern="[0-2][0-9]:[0-5][0-9]" placeholder="HH:MM" required>
    

    <label for="description">Scurtă descriere:</label>
    <textarea id="description" name="description"></textarea>
    
    <input type="submit" value="Trimite">
  </form>
</div>
<script>
$(document).ready(function(){
    $('#booking-form').on('submit', function(e){
    e.preventDefault();
        $.ajax({
            url: 'programare.php',
            method: 'post',
            data: $("#booking-form").serialize(),
            dataType: 'json',
            success: function(data) {
                var response = JSON.parse(data);
                if(response.status == 'success') {
                    alert(response.message);  // Acesta este pop-up-ul pe care îl vrei
                } else {
                    alert('Error: ' + response.message);
                }
            },
            error: function(jqXHR, textStatus, errorThrown){
               alert('Error: ' + textStatus + ' ' + errorThrown);
            }
        });
    });
});


</script>
<!--contact section starts-->
<section class="contact" id="contact">

    <div class="image">
        <img src="hero.png" alt="">
    </div>

    <form action="contact.php" method="POST">
        <h3>contact us</h3>
        <input type="text" name="name" placeholder="your name" class="box">
        <input type="email" name="email" placeholder="your email" class="box">
        <input type="tumber" name="phone" placeholder="your number" class="box">
        <textarea name="message" placeholder="your message" id="" cols="30" rows="10"></textarea>
        <input type="submit" value="send message" class="btn">
    </form>

</section>


<footer class="footer">
  
    <div class="footer-info">
      
      <h2>Happy Paws</h2>
      <p>Ne dedicăm să oferim cele mai bune servicii pentru a vă ajuta să vă îngrijiți de animalele dumneavoastră de companie. Ne pasă de bunăstarea lor și vrem să vă asigurăm că sunt în cele mai bune mâini.</p>
    </div>
    <div class="footer-links">
      <h3>Link-uri utile</h3>
      <ul>
        <li><a href="index.html">Acasă</a></li>
        <li><a href="about.html">Despre noi</a></li>
        <li><a href="preturi.html">Servicii/Tarife</a></li>
        <li><a href="cont.html">Contul meu</a></li>
        <li><a href="contact.html">Contact</a></li>
      </ul>
    </div>
    <div class="footer-social">
      <h3>Urmăriți-ne</h3>
      <ul>
        <li><a href="#"><i class="fab fa-facebook"></i> Facebook</a></li>
        <li><a href="#"><i class="fab fa-instagram"></i> Instagram</a></li>
        <li><a href="#"><i class="fab fa-twitter"></i> Twitter</a></li>
        <li><a href="#"><i class="fab fa-linkedin"></i> LinkedIn</a></li>
      </ul>
    </div>
    <div class="footer-contact">
      <h3>Contact</h3>
      <p><i class="fa-solid fa-envelope"></i> contact@happypaws.com</p>
      <p><i class="fa-solid fa-phone"></i> +123456789</p>
      <p><i class="fa-solid fa-map-marker-alt"></i> Str. Exemplu, Nr. 1, Oras, Judet, Romania</p>
    </div>
  </div>
  <div class="footer-bottom">
    <p>&copy; 2023 Happy Paws. Toate drepturile rezervate.</p>
  </div>
</footer>

 <!-- jquery cdn link -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.4/jquery.min.js"></script>
    <!-- swiper js link -->
    <script src="https://cdn.jsdelivr.net/npm/swiper@9/swiper-bundle.min.js"></script>
    <!-- custom js link -->
    <script src="script.js"></script>
      
    
    
</body>
</html>

