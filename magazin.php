<?php
session_start();

// Verifică dacă utilizatorul este autentificat
if (!isset($_SESSION['userID'])) {
    // Daca utilizatorul nu este autentificat, redirectionati-l catre pagina de login
    header("Location: login.php");
    exit();
}

// Recuperează datele trimise de formular
$productId = $_POST['product_id'];
$quantitySold = $_POST['quantity'];

// Stabiliți conexiunea la baza de date
$conn = new mysqli("localhost", "root", "", "licenta");

// Verificați dacă conexiunea a reușit
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

// Înregistrați vânzarea
$stmt = $conn->prepare("INSERT INTO Vanzari (ProductID, UserID, QuantitySold, SaleDate) VALUES (?, ?, ?, NOW())");
$stmt->bind_param("iii", $productId, $_SESSION['userID'], $quantitySold);
$stmt->execute();

// Actualizați stocul
$stmt = $conn->prepare("UPDATE Produse SET Quantity = Quantity - ? WHERE ProductID = ?");
$stmt->bind_param("ii", $quantitySold, $productId);
$stmt->execute();

// Închideți conexiunea
$conn->close();

// Redirecționare către pagina produsului
header("Location: magazinCOD.php");
exit();
?>
