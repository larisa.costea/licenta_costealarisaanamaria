<!DOCTYPE html>
<html lang="en">
<head>
   <meta charset="UTF-8">
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <meta name="viewport" content="width=device-width, initial-scale=1.0">
   <title>Introducere date livrare</title>
   <!-- Adaugă stilurile CSS aferente paginii de introducere a datelor de livrare -->
   <link rel="stylesheet" href="style_livrare.css">
</head>
<body>
   <h1>Introducere date livrare</h1>
   <form id="delivery-form" action="procesare_livrare.php" method="POST">
      <!-- Adaugă câmpurile necesare pentru introducerea datelor de livrare -->
      <label for="first-name">Nume:</label>
      <input type="text" id="first-name" name="nume" required>

      <label for="last-name">Prenume:</label>
      <input type="text" id="last-name" name="prenume" required>

      <label for="address">Adresă:</label>
      <input type="text" id="address" name="adresa" required>

      <label for="city">Oraș:</label>
      <input type="text" id="city" name="oras" required>

      <label for="postal-code">Cod poștal:</label>
      <input type="text" id="postal-code" name="cod_postal" required>

      <label for="phone">Număr de telefon:</label>
      <input type="text" id="phone" name="telefon" required>

      <label for="email">Email:</label>
      <input type="email" id="email" name="email" required>

      <label for="metoda-plata">Metodă de plată:</label>
      <select id="metoda-plata" name="metoda_plata" required>
         <option value="">-- Selectați o metodă de plată --</option>
         <option value="card">Card bancar</option>
         <option value="ramburs">Plată la livrare (ramburs)</option>
         <option value="online">Plată online</option>
      </select>
      <label for="total-plata">Total plata:</label>
      <input type="text" id="total-plata" name="total_plata" readonly required>

      <label for="produse">Produse:</label>
<textarea id="produse" name="produse_cumparate" readonly></textarea>




      <button type="submit">Finalizare comandă</button>
   </form>
   <div id="error-messages">
      <!-- Mesajele de eroare vor fi afișate aici -->
   </div>
   <?php
   session_start();
   if (isset($_SESSION['comanda_finalizata']) && $_SESSION['comanda_finalizata'] === true) {
       $idLivrare = $_SESSION['id_livrare'];
       ?>
       <form action="generare_factura.php" method="POST">
           <input type="hidden" name="id_livrare" value="<?php echo $idLivrare; ?>">
           <button type="submit">Generare factură</button>
       </form>
       <?php
   }
   ?>
   

<script>
window.onload = function() {
    const total = localStorage.getItem('total');
    if (total) {
        document.getElementById('total-plata').value = total;
        // după ce am folosit totalul, îl ștergem din localStorage
        localStorage.removeItem('total');
    }
}

window.onload = function() {
    const total = localStorage.getItem('total');
    if (total) {
        document.getElementById('total-plata').value = total;
        // după ce am folosit totalul, îl ștergem din localStorage
        localStorage.removeItem('total');
    }

    // Presupunând că ai un array 'cart' cu produsele din coșul de cumpărături
    const cart = JSON.parse(localStorage.getItem('cart')) || [];
    let cartText = '';
    for (let product of cart) {
      cartText += 'Numele produsului: '+ product.ProductName + ', cantitatea:  ' + product.quantity + ', prețul: ' + product.Price + '\n';
    }
    document.getElementById('produse').value = cartText;
    localStorage.removeItem('cart'); // Înlăturăm și produsele după utilizare
}

document.getElementById('delivery-form').addEventListener('submit', function(event) {
      // Opriți acțiunea implicită de trimitere a formularului
      event.preventDefault();

      // Afișează mesajul de confirmare utilizând alert()
      alert('Comanda a fost finalizată cu succes! :)');
   });
</script>


   <div id="error-messages">
      <!-- Mesajele de eroare vor fi afișate aici -->
   </div>
   <!-- Adaugă fișierul JS pentru validarea formularului de livrare -->
   <script src="validare_livrare.js"></script>
</body>
</html>
