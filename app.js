window.onscroll = () => {
    let loginForm = document.querySelector(".login-form");
    let header = document.querySelector('.header');
    let navbar = document.querySelector('.navbar');
    let shoppingCart = document.querySelector('.shopping');
  
    document.querySelector("#login-btn").onclick = () => {
      loginForm.classList.toggle('active');
      navbar.classList.remove('active');
    }
  
    if (window.scrollY > 60) {
      header.classList.add('active');
      shoppingCart.classList.add('hidden');
      navbar.classList.add('hidden');
    } else {
      header.classList.remove('active');
      shoppingCart.classList.remove('hidden');
      navbar.classList.remove('hidden');
    }
  
    
  }
  
  let openShopping = document.querySelector('.shopping');
  let closeShopping = document.querySelector('.closeShopping');
  let list = document.querySelector('.list');
  let listCard = document.querySelector('.listCard');
  let body = document.querySelector('body');
  let total = document.querySelector('.total');
  let quantity = document.querySelector('.quantity');
  let navbar = document.querySelector('.navbar');
  let buyButton = document.querySelector('.buyButton');
  let logoutButton = document.querySelector('.btn-logout'); // selectează butonul de logout

 

  openShopping.addEventListener('click', () => {
    body.classList.add('active');
    navbar.classList.add('hidden'); // ascunde navbar-ul când coșul este deschis
    logoutButton.classList.add('hidden'); // ascunde butonul de logout când coșul este deschis
});
  
closeShopping.addEventListener('click', () => {
    body.classList.remove('active');
    navbar.classList.remove('hidden'); // arată navbar-ul când coșul este închis
    logoutButton.classList.remove('hidden'); // arată butonul de logout când coșul este închis
});
  buyButton.addEventListener('click', finalizareComanda);

  let listCards = [];
  let products = [];
  
  function initApp() {
    fetch('get_cart_content.php')
      .then(response => response.json())
      .then(fetchedProducts => {
        products = fetchedProducts;
        products.forEach((product, key) => {
          let newDiv = document.createElement('div');
          newDiv.classList.add('item');
          newDiv.innerHTML = `
            <img src="${product.ImagePath}">
            <div class="title">${product.ProductName}</div>
            <div class="description">${product.Description}</div>
            <div class="price">${product.Price.toLocaleString()}</div>
            <div class="category">${product.Category}</div>
            <button onclick="addToCard(${key})">Adaugă produsul</button>`;
          list.appendChild(newDiv);
        })
      })
      .catch(error => console.error('Error:', error));
  }

  initApp();
  
  function addToCard(key) {
    fetch('php/is_logged_in.php')
      .then(response => response.json())
      .then(data => {
        if (data.is_logged_in) {
          if (listCards[key] == null) {
            listCards[key] = { ...products[key], quantity: 1 };
          } else {
            listCards[key].quantity += 1;
          }
          reloadCard();
        } else {
          alert('Vă rugăm să vă autentificați.');
        }
      })
      .catch(error => console.error('Error:', error));
  }
  
 
  
  function reloadCard() {
    listCard.innerHTML = '';
    let count = 0;
    let totalPrice = 0;
    listCards.forEach((product, key) => {
      if (product != null) {
        let newDiv = document.createElement('li');
        newDiv.innerHTML = `
          <div><img src="${product.ImagePath}"/></div>
          <div>${product.ProductName}</div>
          <div>${product.Price.toLocaleString()}</div>
          <div>
            <button onclick="changeQuantity(${key}, ${product.quantity - 1})">-</button>
            <div class="count">${product.quantity}</div>
            <button onclick="changeQuantity(${key}, ${product.quantity + 1})">+</button>
          </div>`;
        listCard.appendChild(newDiv);
        count += product.quantity;
        totalPrice += product.Price * product.quantity;
      }
    });
    total.innerText = totalPrice.toLocaleString();
    quantity.innerText = count;
    // presupunând că "totalPrice" este variabila în care se calculează suma totală
localStorage.setItem('total', totalPrice);

  function saveCartToLocalStorage() {
    const cart = Object.values(listCards);
    localStorage.setItem('cart', JSON.stringify(cart));
  }
  
  // Apelul funcției de salvare ar trebui să fie realizat ori de câte ori coșul este actualizat:
  // după adăugarea unui produs în coș, după ștergerea unui produs, după modificarea cantității etc.
  saveCartToLocalStorage();
  
}
  
  function changeQuantity(key, newQuantity) {
    if (newQuantity == 0) {
      delete listCards[key];
    } else {
      listCards[key].quantity = newQuantity;
    }
    reloadCard();
  }

  // codul precedent este neschimbat

// Restul codului JavaScript rămâne neschimbat

function finalizareComanda() {
  // Obțineți lista de comenzi din obiectul listCards
  const comenzi = Object.values(listCards);

  // Verificați dacă există comenzi de procesat
  if (comenzi.length === 0) {
    alert("Nu aveți produse în coșul de cumpărături.");
    return;
  }

  // Redirecționați utilizatorul către pagina de introducere a datelor de livrare
  window.location.href = "pagina_livrare.php";
}


  
  /*functia de cautare */

  document.getElementById("searchButton").addEventListener("click", function(){
    var searchTerm = document.getElementById("searchbarM").value.toLowerCase();
    var filter = document.getElementById("filter").value;

    var productsToDisplay = products.filter(function(product) {
        var productCategoryMatch = (filter === "all" || product.Category === filter);
        var productNameMatch = product.ProductName.toLowerCase().includes(searchTerm);
        return productCategoryMatch && productNameMatch;
    });

    displayProducts(productsToDisplay);
});

function displayProducts(productsToDisplay) {
    // Începe prin a șterge toate produsele afișate
    list.innerHTML = '';

    // Apoi, afișează doar produsele care corespund cu criteriile de căutare
    productsToDisplay.forEach((product, key) => {
        let newDiv = document.createElement('div');
        newDiv.classList.add('item');
        newDiv.innerHTML = `
            <img src="${product.ImagePath}">
            <div class="title">${product.ProductName}</div>
            <div class="description">${product.Description}</div>
            <div class="price">${product.Price.toLocaleString()}</div>
            <div class="category">${product.Category}</div>
            <button onclick="addToCard(${key})">Adaugă produsul</button>`;
        list.appendChild(newDiv);
    });
}
document.getElementById("searchbarM").addEventListener("keydown", function(event){
    if (event.key === "Enter") {
        document.getElementById("searchButton").click();
    }
});
/*filtrare*/
document.getElementById("filter").addEventListener("change", function(){
    var filter = this.value;
    var searchTerm = document.getElementById("searchbarM").value.toLowerCase();

    var productsToDisplay = products.filter(function(product) {
        var productCategoryMatch = (filter === "all" || product.Category === filter);
        var productNameMatch = product.ProductName.toLowerCase().includes(searchTerm);
        return productCategoryMatch && productNameMatch;
    });

    displayProducts(productsToDisplay);
});

function addToFavorites(event) {
    // Obțineți indexul produsului din atributul data-index
    const index = event.target.getAttribute('data-index');
  
    // Obțineți produsul din lista de produse folosind indexul
    const product = products[index];
  
    // Realizați o cerere către server pentru a adăuga produsul în lista de favorite a utilizatorului
    fetch('php/add_to_favorites.php', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({ product }),
    })
      .then(response => response.json())
      .then(data => {
        // Procesați răspunsul de la server (opțional)
        console.log(data);
        // Exemplu: afișați un mesaj de succes
        alert('Produsul a fost adăugat în lista de favorite.');
      })
      .catch(error => {
        console.error('Eroare:', error);
        alert('A apărut o eroare în codul JavaScript. Vă rugăm să verificați consola pentru detalii.');
        throw error; // aruncă eroarea pentru a o afișa în consolă
      });
  }
  
  




 