
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Admin Page</title>
    <!-- CSS stylesheets -->
    <link rel="stylesheet" href="style.css">
    <script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
 <!-- jQuery library -->
 <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <!-- jQuery UI library -->
    <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>

    <style>
         header {
            background-image: url("header-background.jpg");
            background-size: cover;
            background-position: center;
            height: 300px;
            display: flex;
            align-items: center;
            justify-content: center;
            color: #fff;
            text-align: center;
        }
        main {
            display: flex;
            flex-wrap: wrap;
        }

        section {
            flex: 1;
            margin-right: 20px;
            margin-bottom: 20px;
        }
        header {
            background-image: url("image/grup.jpg");
            background-size: cover;
            background-position: center;
            height: 300px;
            display: flex;
            align-items: center;
            justify-content: center;
            color: #fff;
            text-align: center;
        }

        h1 {
            font-size: 36px;
            margin-bottom: 10px;
        }

        p {
            font-size: 18px;
        }
    </style>
</head>
<body>
<?php
session_start();
if (!isset($_SESSION["isAdmin"]) || $_SESSION["isAdmin"] != true) {
    echo "Accesul la această pagină este restricționat.";
    exit;
}
// Restul codului pentru pagina de admin
?>

    <!-- Header section -->
    <header>
        <!-- Navbar code goes here -->
    </header>

    <!-- Main content section -->
    <main>
    <canvas id="delivery-chart"></canvas>


        <!-- Add product form -->
<section id="add-product">
    <h2>Adăugare produs</h2>
    <form action="php/add_product.php" method="POST">
        <label for="product-name">Nume produs:</label>
        <input type="text" name="product-name" id="product-name" required>

        <label for="product-description">Descriere produs:</label>
        <textarea name="product-description" id="product-description" required></textarea>

        <label for="product-price">Preț produs:</label>
        <input type="number" name="product-price" id="product-price" required>

        <label for="product-category">Categorie produs:</label>
        <input type="text" name="product-category" id="product-category" required>

        <label for="product-quantity">Cantitate produs:</label>
        <input type="number" name="product-quantity" id="product-quantity" required>

        <label for="product-image">Imagine produs:</label>
        <input type="text" name="product-image" id="product-image" required>
        <span id="selected-image"></span>

        <button type="submit">Adaugă produs</button>
    </form>
</section>

<script>
    // Ascultați evenimentul de schimbare a câmpului de fișier
    document.getElementById("product-image").addEventListener("change", function(event) {
        // Obțineți calea completă a fișierului selectat
        var filePath = event.target.value;

        // Afișați calea completă a fișierului în elementul span
        document.getElementById("selected-image").textContent = "Imagine selectată: " + filePath;
    });
</script>

        <!-- Delete product form -->
        <section id="delete-product">
            <h2>Ștergere produs</h2>
            <form action="php/delete_product.php" method="POST">
                <label for="product-id">ID produs:</label>
                <input type="number" name="product-id" id="product-id" required>
                
                <button type="submit">Șterge produs</button>
            </form>
        </section>

        <!-- Conturi form -->
        <section id="conturi">
            <h3>Gestionare conturi utilizatori</h3>
            <form id="form-conturi" action="php/gestiune_conturi.php" method="POST">
                <!-- Dropdown cu utilizatorii din baza de date -->
                <label for="select-utilizator">Alege utilizatorul:</label>
                <select name="select-utilizator" id="select-utilizator">
                    
                    <?php
                    // Conectare la baza de date și interogare pentru a obține conturile de utilizatori
                    $servername = "localhost";
                    $username = "root";
                    $password = "";
                    $dbname = "licenta";
                    $conn = new mysqli($servername, $username, $password, $dbname);
                    if ($conn->connect_error) {
                        die("Conexiunea la baza de date a eșuat: " . $conn->connect_error);
                    }

                    $query = "SELECT * FROM utilizatori";
                    $result = $conn->query($query);

                    if ($result->num_rows > 0) {
                        while ($row = $result->fetch_assoc()) {
                            echo "<option value='" . $row['id'] . "'>" . $row['nume'] . "</option>";
                        }
                    }

                    $conn->close();
                    ?>
                </select>
                <button type="submit">Șterge utilizator</button>
            </form>
        </section>

        <!-- Mesaje form -->
        <section id="mesaje">
  <h3>Gestionare mesaje</h3>
  <form  id="delete-message-form" action="php/gestiune_mesaje.php" method="POST">
    <!-- Dropdown cu mesajele din baza de date -->
    <label for="select-mesaj">Alege mesajul:</label>
    <select name="message-id" id="select-mesaj">
    <?php
      // Conectare la baza de date și interogare pentru a obține mesajele
      $servername = "localhost";
      $username = "root";
      $password = "";
      $dbname = "licenta";
      $conn = new mysqli($servername, $username, $password, $dbname);
      if ($conn->connect_error) {
        die("Conexiunea la baza de date a eșuat: " . $conn->connect_error);
      }

      $query = "SELECT * FROM mesaje";
      $result = $conn->query($query);

      if ($result->num_rows > 0) {
        while ($row = $result->fetch_assoc()) {
          echo "<option value='" . $row['id'] . "'>" . $row['mesaj'] . "</option>";
        }
      }

      $conn->close();
    ?>
    </select>
    <button type="submit" name="delete">Șterge mesaj</button>
  </form>
  <?php
    if (isset($_GET['msg'])) {
      echo "<p>" . $_GET['msg'] . "</p>";
    }
  ?>
</section>


        <!-- Programari form -->
        <section id="programari">
            <h3>Gestionare programări</h3>
            <form id="delete-booking-form" action="php/gestiune_programare.php" method="POST">
                <!-- Dropdown cu programările din baza de date -->
                <label for="select-programare">Alege programarea:</label>
                <select name="booking-id" id="select-programare">
                    <?php
                    // Conectare la baza de date și interogare pentru a obține programările
                    $servername = "localhost";
                    $username = "root";
                    $password = "";
                    $dbname = "licenta";
                    $conn = new mysqli($servername, $username, $password, $dbname);
                    if ($conn->connect_error) {
                        die("Conexiunea la baza de date a eșuat: " . $conn->connect_error);
                    }

                    $query = "SELECT * FROM bookings";
                    $result = $conn->query($query);

                    if ($result->num_rows > 0) {
                        while ($row = $result->fetch_assoc()) {
                            echo "<option value='" . $row['id'] . "'>" . $row['data_programare'] . "</option>";
                        }
                    }

                    $conn->close();
                    ?>
                </select>
                <button type="submit">Șterge programare</button>
            </form>
        </section>

        <!-- Modificare produs form -->
<section id="modificare-produs">
    <h3>Modificare produs</h3>
    <form action="php/modificare_produs.php" method="POST">
        <!-- Dropdown cu produsele din baza de date -->
        <label for="select-produs">Alege produsul:</label>
        <select name="product-id" id="select-produs">

            <?php
            // Conectare la baza de date și interogare pentru a obține produsele
            $servername = "localhost";
            $username = "root";
            $password = "";
            $dbname = "licenta";
            $conn = new mysqli($servername, $username, $password, $dbname);
            if ($conn->connect_error) {
                die("Conexiunea la baza de date a eșuat: " . $conn->connect_error);
            }

            $query = "SELECT * FROM produse";
            $result = $conn->query($query);

            if ($result->num_rows > 0) {
                while ($row = $result->fetch_assoc()) {
                    echo "<option value='" . $row['ProductID'] . "'>" . $row['ProductName'] . "</option>";
                }
            }

            $conn->close();
            ?>
        </select>

        
        <!-- Câmpuri pentru modificarea datelor produsului -->
        <label for="product-name">Nume produs:</label>
        <input type="text" name="product-name" id="product-name" required>
        
        <label for="product-description">Descriere produs:</label>
        <textarea name="product-description" id="product-description" required></textarea>
        
        <label for="product-price">Preț produs:</label>
        <input type="number" name="product-price" id="product-price" required>

        <label for="product-category">Categorie produs:</label>
        <input type="text" name="product-category" id="product-category" required>
                
        <label for="product-quantity">Cantitate produs:</label>
        <input type="number" name="product-quantity" id="product-quantity" required>


        <button type="submit">Modifică produs</button>
    </form>
</section>

<div class="table-container">
<!-- Programari form -->
<section id="vizualizare-programari">
    <h3>Vizualizare programări</h3>
    <table id="myTable">
        <tr>
            <th>ID</th>
            <th>Data</th>
            <th>Ora</th>
            <th>Utilizator</th>
            <th>Animal</th>
        </tr>

        <?php
        // Conectare la baza de date și interogare pentru a obține programările
        $servername = "localhost";
        $username = "root";
        $password = "";
        $dbname = "licenta";
        $conn = new mysqli($servername, $username, $password, $dbname);
        if ($conn->connect_error) {
            die("Conexiunea la baza de date a eșuat: " . $conn->connect_error);
        }

        $query = "SELECT * FROM bookings";
        $result = $conn->query($query);

        if ($result->num_rows > 0) {
            while ($row = $result->fetch_assoc()) {
                echo "<tr>";
                echo "<td>" . $row['id'] . "</td>";
                echo "<td>" . $row['data_programare'] . "</td>";
                echo "<td>" . $row['ora_programare'] . "</td>";
                echo "<td>" . $row['nume'] . "</td>";
                echo "<td>" . $row['tip_animal'] . "</td>";
                echo "</tr>";
            }
        }

        $conn->close();
        ?>
        
    </table>
    <!-- Livrari form -->
<section id="livrari">
    <h3>Gestionare livrări</h3>
    <table id="livrari-table">
        <tr>
            <th>ID</th>
            <th>Nume</th>
            <th>Prenume</th>
            <th>Adresă</th>
            <th>Oraș</th>
            <th>Cod Poștal</th>
            <th>Telefon</th>
            <th>Email</th>
            <th>Metodă de plată</th>
            <th>Total plată</th>
            <th>Data livrare</th> 
            <th style="width: 200px;">Produse Cumpărate</th> 

        </tr>

        <?php
        // Conectare la baza de date și interogare pentru a obține livrările
        $servername = "localhost";
        $username = "root";
        $password = "";
        $dbname = "licenta";
        $conn = new mysqli($servername, $username, $password, $dbname);
        if ($conn->connect_error) {
            die("Conexiunea la baza de date a eșuat: " . $conn->connect_error);
        }

        $query = "SELECT * FROM livrari";
        $result = $conn->query($query);

        if ($result->num_rows > 0) {
            while ($row = $result->fetch_assoc()) {
                echo "<tr>";
                echo "<td>" . $row['id'] . "</td>";
                echo "<td>" . $row['nume'] . "</td>";
                echo "<td>" . $row['prenume'] . "</td>";
                echo "<td>" . $row['adresa'] . "</td>";
                echo "<td>" . $row['oras'] . "</td>";
                echo "<td>" . $row['cod_postal'] . "</td>";
                echo "<td>" . $row['telefon'] . "</td>";
                echo "<td>" . $row['email'] . "</td>";
                echo "<td>" . $row['metoda_plata'] . "</td>";
                echo "<td>" . $row['total_plata'] . "</td>";
                echo "<td>" . $row['data_livrare'] . "</td>";
                echo "<td>" . $row['produse_cumparate'] . "</td>"; // Afiseaza data_livrare
                echo "</tr>";
            }
        }

        $conn->close();
        ?>
    </table>
</section>


<style>
    #livrari-table {
        font-family: Arial, sans-serif;
        border-collapse: collapse;
        width: 100%;
    }

    #livrari-table td, #livrari-table th {
        border: 1px solid #ddd;
        padding: 8px;
    }

    #livrari-table tr:nth-child(even) {
        background-color: #f2f2f2;
    }

    #livrari-table tr:hover {
        background-color: #ddd;
    }

    #livrari-table th {
        padding-top: 12px;
        padding-bottom: 12px;
        text-align: left;
        background-color: var(--secondary);
        color: white;
    }
</style>

    <style>
    #myTable {
        font-family: Arial, sans-serif;
        border-collapse: collapse;
        width: 100%;
    }

    #myTable td, #myTable th {
        border: 1px solid #ddd;
        padding: 8px;
    }

    #myTable tr:nth-child(even) {
        background-color: #f2f2f2;
    }

    #myTable tr:hover {
        background-color: #ddd;
    }

    #myTable th {
        padding-top: 12px;
        padding-bottom: 12px;
        text-align: left;
        background-color: var(--secondary);
        color: white;
    }
</style>

</section>

    </div>
    <section>
   
<h3>Vizualizare conturi</h3>
<table id="myTable" >
    <tr>
        <th>ID</th>
        <th>Nume</th>
        <th>Email</th>
        <!-- Și orice alte coloane dorești să afișezi -->
    </tr>
    <?php
        // Conectare la baza de date și interogare pentru a obține conturile de utilizatori
        $servername = "localhost";
        $username = "root";
        $password = "";
        $dbname = "licenta";
        $conn = new mysqli($servername, $username, $password, $dbname);

        if ($conn->connect_error) {
            die("Conexiunea la baza de date a eșuat: " . $conn->connect_error);
        }

        $query = "SELECT * FROM utilizatori";
        $result = $conn->query($query);

        if ($result->num_rows > 0) {
            while ($row = $result->fetch_assoc()) {
                echo "<tr>";
                echo "<td>" . $row['id'] . "</td>";
                echo "<td>" . $row['nume'] . "</td>";
                echo "<td>" . $row['email'] . "</td>";
                // Și orice alte coloane dorești să afișezi
                echo "</tr>";
            }
        }

        $conn->close();
    ?>
</table>


</section>
    </main>

    <!-- Footer section -->
    <footer>
        <!-- Footer content goes here -->
    </footer>

    <!-- JavaScript code goes here -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script>
    /*sterge cont */
    $(document).ready(function() {
    $("#form-conturi").on('submit', function(e) {
        e.preventDefault();

        var user_id = $("#select-utilizator").val();

        if(confirm('Ești sigur că vrei să ștergi acest utilizator?')) {
            $.ajax({
                type: "POST",
                url: "php/gestiune_conturi.php",
                data: { 'user-id': user_id },
                success: function(result) {
                    alert(result);
                },
                error: function(result) {
                    alert('error');
                }
            });
        }
    });
});

/*stergere programare */
$(document).ready(function() {
    $("#delete-booking-form").on('submit', function(e) {
        e.preventDefault();

        var booking_id = $("#select-programare").val();

        if(confirm('Ești sigur că vrei să ștergi această programare?')) {
            $.ajax({
                type: "POST",
                url: "php/gestiune_programare.php",
                data: { 'booking-id': booking_id },
                success: function(result) {
                    alert(result);
                },
                error: function(result) {
                    alert('error');
                }
            });
        }
    });
});


    /*sterge mesaj */
    $(document).ready(function() {
    $("#delete-message-form").on('submit', function(e) {
        e.preventDefault();

        var message_id = $("#select-mesaj").val();

        if(confirm('Ești sigur că vrei să ștergi acest mesaj?')) {
            $.ajax({
                type: "POST",
                url: "php/gestiune_mesaje.php",
                data: { 'message-id': message_id },
                success: function(result) {
                    alert(result);
                },
                error: function(result) {
                    alert('error');
                }
            });
        }
    });
});



    /*form adauga produs */
$(document).ready(function(){
    $("#add-product form").on("submit", function(e){
        e.preventDefault(); // previne formularul de la trimitere normală
        var formData = $(this).serialize(); // preia datele din formular
        $.post("php/add_product.php", formData, function(data){
            // în data se va regăsi răspunsul de la server
            alert(data); // arată un mesaj cu răspunsul de la server
        });
    });
});

/*form modificare produs */
$(document).ready(function(){
    // Atunci când produsul selectat se schimbă în formularul de modificare a produsului
    $("#modificare-produs #select-produs").change(function(){
        var productId = $(this).val(); // Ia ID-ul produsului selectat
        var imageUrl = $(this).find(':selected').data('image-url');
        // Trimite o cerere AJAX la server pentru a obține datele produsului
        $.ajax({
            url: 'php/get_product.php', // Acest script PHP va trebui să returneze datele produsului în format JSON
            method: 'POST',
            data: { id: productId },
            dataType: 'json',
            success: function(data) {
                // Când datele vin de la server, completează automat formularele de modificare a produsului
                $("#modificare-produs #product-name").val(data.ProductName);
                $("#modificare-produs #product-description").val(data.Description);
                $("#modificare-produs #product-price").val(data.Price);
                $("#modificare-produs #product-category").val(data.Category);
                $("#modificare-produs #product-quantity").val(data.Quantity);
                $("#modificare-produs #product-image").val(imageUrl);
            }
        });
    });
});

$(document).ready(function(){
    $("#modificare-produs form").on("submit", function(e){
        e.preventDefault(); // previne formularul de la trimitere normală
        var formData = $(this).serialize(); // preia datele din formular
        $.post("php/modificare_produs.php", formData, function(data){
            // în data se va regăsi răspunsul de la server
            alert(data); // arată un mesaj cu răspunsul de la server
            
            // Actualizează conținutul paginii pentru a reflecta modificările făcute
            // Aici poți adăuga codul necesar pentru actualizarea paginii cu noile date
            
        });
    });
});
/**grafic admin */
// Obțineți elementul canvas și contextul de desenare
var chartCanvas = document.getElementById('delivery-chart').getContext('2d');

// Obțineți datele din tabela de livrări prin intermediul unei cereri AJAX către server
$.ajax({
    url: 'afisare_livrari.php', // Schimbați acest URL cu calea către scriptul PHP care va prelua datele din tabela de livrări
    method: 'POST',
    dataType: 'json',
    success: function(data) {
        // Extrageți datele necesare pentru grafic
        var deliveryDates = data.map(function(item) {
            return item.data_livrare;
        });

        // Creați un obiect de tip Set pentru a obține toate datele unice
        var uniqueDates = new Set(deliveryDates);

        // Obțineți frecvența pentru fiecare dată unică
        var dateFrequency = {};
        deliveryDates.forEach(function(date) {
            dateFrequency[date] = (dateFrequency[date] || 0) + 1;
        });

        // Obțineți etichetele și datele pentru grafic
        var chartLabels = Array.from(uniqueDates);
        var chartData = chartLabels.map(function(date) {
            return dateFrequency[date];
        });

        // Configurați graficul utilizând datele și etichetele
        var deliveryChart = new Chart(chartCanvas, {
    type: 'bar',
    data: {
        labels: chartLabels,
        datasets: [{
            label: 'Frecvență livrări',
            data: chartData,
            backgroundColor: 'rgba(54, 162, 235, 0.5)', // Culoarea de fundal a barelor
            borderColor: 'rgba(54, 162, 235, 1)', // Culoarea conturului barelor
            borderWidth: 1
        }]
    },
    options: {
        maintainAspectRatio: true,
        aspectRatio: 3.3, // Adăugați această linie pentru a controla raportul de aspect
        width: 100, // Adăugați această linie pentru a controla lățimea graficului
        height: 50 // Adăugați această linie pentru a controla înălțimea graficului
    }
});

    },
    error: function() {
        console.log('Eroare la obținerea datelor de livrare');
    }
});


</script>

</body>
</html>
