<?php
session_start();
$conn = mysqli_connect('localhost','root','','licenta') or die('connection failed');

// Verifică dacă utilizatorul este autentificat
if (!isset($_SESSION['isLogged']) || $_SESSION['isLogged'] !== true) {
  // Utilizatorul nu este autentificat
  echo json_encode(['success' => false, 'message' => 'Utilizatorul nu este autentificat.']);
  exit;
}

// Obțineți lista de comenzi din corpul cererii
$data = json_decode(file_get_contents('php://input'), true);
$comenzi = $data['comenzi'];

// Verificați dacă există comenzi de procesat
if (empty($comenzi)) {
  echo json_encode(['success' => false, 'message' => 'Nu există produse în coșul de cumpărături.']);
  exit;
}

// Obțineți ID-ul utilizatorului din sesiune
$id_utilizator = $_SESSION['id_utilizator'];

// Obțineți data curentă
$data_vanzarii = date('Y-m-d');

try {
  // Începeți o tranzacție
  $db->beginTransaction();

  // Iterați prin fiecare comandă și inserați-o în tabelul "vanzari"
  foreach ($comenzi as $comanda) {
    $id_produs = $comanda['id_produs'];
    $id_comanda = $comanda['id_comanda'];
    $cantitate_vanduta = $comanda['quantity'];
    $pret_curent = $comanda['price'];

    // Realizați interogarea de inserare în tabelul "vanzari"
    $stmt = $db->prepare("INSERT INTO vanzari (data_vanzarii, id_utilizator, id_produs, id_comanda, cantitate_vanduta, pret_curent)
                          VALUES (:data_vanzarii, :id_utilizator, :id_produs, :id_comanda, :cantitate_vanduta, :pret_curent)");
    $stmt->bindParam(':data_vanzarii', $data_vanzarii);
    $stmt->bindParam(':id_utilizator', $id_utilizator);
    $stmt->bindParam(':id_produs', $id_produs);
    $stmt->bindParam(':id_comanda', $id_comanda);
    $stmt->bindParam(':cantitate_vanduta', $cantitate_vanduta);
    $stmt->bindParam(':pret_curent', $pret_curent);
    $stmt->execute();
  }

  // Confirmați tranzacția
  $db->commit();

  // Răspunde cu un mesaj de succes
  echo json_encode(['success' => true, 'message' => 'Comanda a fost plasată cu succes!']);
} catch (Exception $e) {
  // Anulați tranzacția în caz de eroare
  $db->rollBack();

  // Răspunde cu un mesaj de eroare
  echo json_encode(['success' => false, 'message' => 'A apărut o eroare în procesarea comenzii. Vă rugăm să încercați din nou mai târziu.']);
}
?>
