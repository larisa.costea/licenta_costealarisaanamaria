<?php
$conn = mysqli_connect("localhost", "root", "", "licenta");

// Verificați conexiunea
if (!$conn) {
    die("Conexiunea la baza de date a eșuat: " . mysqli_connect_error());
}

$sql = "SELECT * FROM livrari ORDER BY data_livrare DESC LIMIT 60";
$result = mysqli_query($conn, $sql);

// Verificați dacă interogarea a returnat rezultate
if (mysqli_num_rows($result) > 0) {
    $data = array();

    while ($row = mysqli_fetch_assoc($result)) {
        $data[] = array(
            'id' => $row['id'],
            'nume' => $row['nume'],
            'prenume' => $row['prenume'],
            'adresa' => $row['adresa'],
            'oras' => $row['oras'],
            'cod_postal' => $row['cod_postal'],
            'telefon' => $row['telefon'],
            'email' => $row['email'],
            'metoda_plata' => $row['metoda_plata'],
            'total_plata' => $row['total_plata'],
            'data_livrare' => $row['data_livrare']
        );
    }

    // Convertiți array-ul în format JSON și returnați-l
    echo json_encode($data);
} else {
    echo "Nu există date în tabela livrari.";
}

mysqli_close($conn);
?>
