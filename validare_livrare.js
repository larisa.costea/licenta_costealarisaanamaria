// Obțineți elementul formularului
const deliveryForm = document.getElementById("delivery-form");

// Ascultați evenimentul de trimitere a formularului
deliveryForm.addEventListener("submit", validateForm);

function validateForm(event) {
  event.preventDefault(); // Opriți comportamentul implicit de trimitere a formularului

  // Resetați mesajele de eroare
  resetErrorMessages();

  // Validați câmpurile formularului
  let isValid = true;

  const firstNameInput = document.getElementById("first-name");
  if (firstNameInput.value.trim() === "") {
    displayErrorMessage(firstNameInput, "Vă rugăm să completați numele.");
    isValid = false;
  }

  // Validați celelalte câmpuri de introducere a datelor de livrare

  // ...

  // Verificați dacă formularul este valid și trimiteți-l
  if (isValid) {
    deliveryForm.submit();
  }
}

function displayErrorMessage(input, message) {
  const errorMessagesElement = document.getElementById("error-messages");
  const errorMessageElement = document.createElement("div");
  errorMessageElement.classList.add("error-message");
  errorMessageElement.textContent = message;
  errorMessagesElement.appendChild(errorMessageElement);
  input.classList.add("invalid");
}

function resetErrorMessages() {
  const errorMessagesElement = document.getElementById("error-messages");
  errorMessagesElement.innerHTML = "";

  const invalidInputs = document.querySelectorAll(".invalid");
  invalidInputs.forEach((input) => {
    input.classList.remove("invalid");
  });
}
