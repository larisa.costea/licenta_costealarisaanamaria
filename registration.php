<?php
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "licenta";

// Crearea conexiunii la baza de date
$conn = new mysqli($servername, $username, $password, $dbname);

// Verificarea conexiunii
if ($conn->connect_error) {
    die("Conexiunea la baza de date a eșuat: " . $conn->connect_error);
}

// Procesarea formularului de înregistrare
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $nume = $_POST["nume"];
    $email = $_POST["email"];
    $parola = $_POST["parola"];

    // Criptarea parolei
    $parola_hash = password_hash($parola, PASSWORD_DEFAULT);

    // Inserarea noului utilizator în baza de date
    $inserare_utilizator = "INSERT INTO utilizatori (nume, email, parola) VALUES ('$nume', '$email', '$parola_hash')";
    if ($conn->query($inserare_utilizator) === TRUE) {
        $successMessage = "Înregistrarea a fost efectuată cu succes!";
    } else {
        echo "Eroare la înregistrare: " . $conn->error;
    }
}

// Închiderea conexiunii la baza de date
$conn->close();
?>
