
<?php
session_start();

// Verificați dacă produsul există deja în coș
if (isset($_SESSION['cart'][$product_id])) {
  // Produsul există deja în coș, actualizați cantitatea
  $_SESSION['cart'][$product_id] += $quantity;
} else {
  // Produsul nu există în coș, adăugați-l
  $_SESSION['cart'][$product_id] = $quantity;
}

// Răspunsul poate fi personalizat în funcție de necesitățile dvs.
echo "Produsul a fost adăugat în coșul de cumpărături.";
?>



