<?php 
session_start();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Happy Paws</title>
    <!-- font awesome link-->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css">
    <!-- swiper css link -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/swiper@9/swiper-bundle.min.css" />
    <!-- custom css link -->
    <link rel="stylesheet" href="style.css">
   
    


</head>
<body>
  <header class="header">
    <a href="index.php" class="logo"> <i class="fa-solid fa-paw"></i>Happy Paws</a>
    <nav class="navbar">
        <a href="index.php">Acasa</a>
        <a href="about.php">Despre Noi</a>
        <a href="preturi.php">Servicii/Tarife</a>
        <a class="active" href="cont.php">Contul Meu</a>
        <a href="contactCOD.php">Contact</a>
        <a  href="blog.php">Blog</a>
        <a href="test.php">Produse</a>
        <?php
if (isset($_SESSION["isAdmin"]) && $_SESSION["isAdmin"] == true) {
echo '<a href="admin.php">Panou Admin</a>';
}
?>
    </nav>
    <div class="icons">
        <div id="login-btn" class="fas fa-user"></div>
        <div id="menu-btn" class="fas fa-bars"></div>
        
    </div>
    <!-- login form-->
    <?php
if (isset($_SESSION["isLogged"]) && $_SESSION["isLogged"] == true) {
// Utilizatorul este autentificat
echo '<a href="logout.php" class="btn btn-logout">Logout</a>';
} else {
// Utilizatorul nu este autentificat
?>
    <form action="login.php" method="POST" class="login-form">
        <h3>login form <i class="fa-solid fa-paw"></i></h3>
        <input type="email" name="email" placeholder="Introduceti adresa de email" class="box">
        <input type="password" name="parola" placeholder="Introduceti parola" class="box">
        <div class="remember">
            <input type="checkbox" name="remember" id="remember-me">
            <label for="remember-me"> remember me</label>
        </div>
        <label for="rol">Selectează rolul:</label>
        <select name="rol" id="rol">
            <option value="user">Utilizator</option>
            <option value="admin">Admin</option>
        </select>
        <button type="submit" class="btn">login</button>
    </form>
    <?php
}
?>
</header>
<!--header section ends -->

<!--home section starts-->
<section class="cont" id="cont">
    <div class="content" >
        <h3> Contul meu </h3>
        
    </div>
    
    <div class="custom-shape-divider-bottom-1684086027">
        <svg data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1200 120" preserveAspectRatio="none">
            <path d="M321.39,56.44c58-10.79,114.16-30.13,172-41.86,82.39-16.72,168.19-17.73,250.45-.39C823.78,31,906.67,72,985.66,92.83c70.05,18.48,146.53,26.09,214.34,3V0H0V27.35A600.21,600.21,0,0,0,321.39,56.44Z" class="shape-fill"></path>
        </svg>
    </div>
     </section>
<!-- home section ends-->
<div class="container-form">
    <div class="account">
        <div class="account__tabs">
            <button class="tablink active" onclick="openTab('Login', this)" id="defaultOpen">Autentificare</button>
            <button class="tablink" onclick="openTab('Register', this)">Înregistrare</button>
            <button class="tablink" onclick="openTab('Forgot', this)">Ai uitat parola?</button>
        </div>

        <div id="Login" class="tabcontent active">
            <h2><i class="fa-solid fa-paw"></i>Autentificare</h2>
            <form action="login.php" method="POST">
                <input type="email" name="email" placeholder="Email" required>
                <input type="password" name="parola"placeholder="Parola" required>
                <label for="rol" >Selectează rolul:</label>
    <select name="rol" id="rol">
        <option value="user">Utilizator</option>
        <option value="admin">Admin</option>
    </select>
                <button type="submit"></i>Autentificare</button>
            </form>
        </div>

        <div id="Register" class="tabcontent">
          <div id="message"></div>
            <h2><i class="fa-solid fa-paw"></i>Înregistrare</h2>
            <form action="registration.php" method="POST">
                <input type="text" name="nume" placeholder="Nume complet" required>
                <input type="email" name="email" placeholder="Email" required>
                <input type="password"name="parola" placeholder="Parola" required>
                <button type="submit" value="Inregistrare">Înregistrare</button>
            </form>
        </div>

        <div id="Forgot" class="tabcontent">
            <h2><i class="fa-solid fa-paw"></i>Resetare parolă</h2>
            <form action="parola.php" method="post">
                <input type="email" name="email" placeholder="Email" required>
                <button type="submit">Trimite</button>
            </form>
        </div>
    </div>
</div>
<script>
    function openTab(tabName, elmnt) {
  var i, tabcontent, tablinks;

  tabcontent = document.getElementsByClassName("tabcontent");
  for (i = 0; i < tabcontent.length; i++) {
    tabcontent[i].style.display = "none";
  }

  tablinks = document.getElementsByClassName("tablink");
  for (i = 0; i < tablinks.length; i++) {
    tablinks[i].classList.remove("button_active");
  }

  document.getElementById(tabName).style.display = "block";
  elmnt.classList.add("button_active");

  // Adăugăm verificarea pentru a închide pagina de autentificare atunci când se face clic pe celelalte tab-uri
  if (tabName !== "Login") {
    document.getElementById("Login").style.display = "none";
    document.getElementById("defaultOpen").classList.remove("button_active");
  }
}

// Simulăm un clic pe butonul "Autentificare" la încărcarea paginii
window.addEventListener("load", function() {
  document.getElementById("defaultOpen").click();
});

</script>

<section class="contact" id="contact">

    <div class="image">
        <img src="hero.png" alt="">
    </div>

    <form action="contact.php" method="POST">
      <h3>Contactează-ne</h3>
      <input type="text" name="name" placeholder="numele dvs." class="box">
      <input type="email" name="email" placeholder="email" class="box">
      <input type="tumber" name="phone" placeholder="număr de telefon" class="box">
      <textarea name="message" placeholder="mesajul dvs." id="" cols="30" rows="10"></textarea>
      <input type="submit" value="Trimite" class="btn">
  </form>


</section>
</section>
<footer class="footer">
  
  <div class="footer-info">
    
    <h2>Happy Paws</h2>
    <p>Ne dedicăm să oferim cele mai bune servicii pentru a vă ajuta să vă îngrijiți de animalele dumneavoastră de companie. Ne pasă de bunăstarea lor și vrem să vă asigurăm că sunt în cele mai bune mâini.</p>
  </div>
  <div class="footer-links">
    <h3>Link-uri utile</h3>
    <ul>
      <li><a href="index.html">Acasă</a></li>
      <li><a href="about.html">Despre noi</a></li>
      <li><a href="preturi.html">Servicii/Tarife</a></li>
      <li><a href="cont.html">Contul meu</a></li>
      <li><a href="contact.html">Contact</a></li>
    </ul>
  </div>
  <div class="footer-social">
    <h3>Urmăriți-ne</h3>
    <ul>
      <li><a href="#"><i class="fab fa-facebook"></i> Facebook</a></li>
      <li><a href="#"><i class="fab fa-instagram"></i> Instagram</a></li>
      <li><a href="#"><i class="fab fa-twitter"></i> Twitter</a></li>
      <li><a href="#"><i class="fab fa-linkedin"></i> LinkedIn</a></li>
    </ul>
  </div>
  <div class="footer-contact">
    <h3>Contact</h3>
    <p><i class="fa-solid fa-envelope"></i> contact@happypaws.com</p>
    <p><i class="fa-solid fa-phone"></i> +123456789</p>
    <p><i class="fa-solid fa-map-marker-alt"></i> Str. Exemplu, Nr. 1, Oras, Judet, Romania</p>
  </div>
</div>
<div class="footer-bottom">
  <p>&copy; 2023 Happy Paws. Toate drepturile rezervate.</p>
</div>
</footer>
  

 <!-- jquery cdn link -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.4/jquery.min.js"></script>
    <!-- swiper js link -->
    <script src="https://cdn.jsdelivr.net/npm/swiper@9/swiper-bundle.min.js"></script>
    <!-- custom js link -->
    <script src="script.js"></script>
</body>
</html>