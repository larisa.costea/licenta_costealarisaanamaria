document.addEventListener("DOMContentLoaded", function () {
  const chatbotIcon = document.querySelector(".chatbot-icon");
  const chatbotContainer = document.querySelector(".chatbot-container");
  const closeBtn = document.querySelector(".close-btn");
  const chatbotContent = document.querySelector(".chatbot-content");
  const chatInput = document.querySelector(".chatbot-input textarea");
  const sendChatBtn = document.querySelector(".chatbot-input span");

  chatbotIcon.addEventListener("click", function () {
    document.body.classList.toggle("show-chatbot");
    chatInput.focus();
  });

  closeBtn.addEventListener("click", function () {
    document.body.classList.remove("show-chatbot");
  });

  sendChatBtn.addEventListener("click", function () {
    const question = chatInput.value.trim();
    if (question !== "") {
      const response = getChatbotResponse(question);
      displayChatMessage(question, "user");
      displayChatMessage(response, "bot");
      chatInput.value = "";
    }
  });

  function displayChatMessage(message, sender) {
    const chatMessage = document.createElement("p");
    chatMessage.classList.add("message", sender);
    chatMessage.textContent = message;
    chatbotContent.appendChild(chatMessage);
    chatbotContent.scrollTop = chatbotContent.scrollHeight;
}


  function getChatbotResponse(question) {
    const qaPairs = {
      "Salut": "Bună! Cum pot să te ajut?",
      "Bună": "Bună! Cum pot să te ajut?",
      "Ce servicii oferiți?": "Oferim servicii medicale veterinare complete, inclusiv consultații, imunizări, intervenții chirurgicale și îngrijire generală.",
      "Care este adresa voastră?": "Ne găsiți la adresa Timișoara, Piața Victorie.",
      "Care este programul de funcționare?":"Suntem deschiși de luni până vineri, de la 9:00 la 18:00, și sâmbătă de la 9:00 la 16:00. Duminica suntem închisi.",
      "Cum pot programa o consultație?": "Puteți programa o consultație sunându-ne sau prin formularul nostru online.",
      "Ce fac dacă animalul meu a înghițit ceva toxic?": "Dacă animalul dvs. a înghițit ceva toxic, contactați-ne imediat sau duceți-l la cea mai apropiată clinică de urgență.",
      "Cât de des ar trebui să aduc animalul meu la control?": "Recomandăm un control de rutină cel puțin o dată pe an pentru a ne asigura că animalul dumneavoastră de companie este sănătos și pentru a preveni eventuale probleme de sănătate.",
      "Oferiți servicii de îngrijire dentară pentru animalele de companie?": "Da, oferim o gamă completă de servicii dentare, inclusiv curățare profesională și extracții dentare dacă este necesar.",
      "Cum pot preveni bolile parazitare la animalul meu de companie?": "Există multe modalități de a preveni bolile parazitare, inclusiv utilizarea de medicamente preventive. Vă recomandăm să aduceți animalul la o consultație pentru a discuta cea mai bună opțiune pentru nevoile specifice ale acestuia.",
      "Mulțumesc":"Cu drag! Nu ezitați să ne contactați pentru orice întrebări aveți!",
      
      // Adăugați mai multe întrebări și răspunsuri aici
    };

    if (qaPairs.hasOwnProperty(question)) {
      return qaPairs[question];
    } else {
      return "Îmi pare rău, nu pot răspunde la această întrebare în acest moment.";
    }
  }
});
