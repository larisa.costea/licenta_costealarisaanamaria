<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

$servername = "localhost";
$username = "root";
$password = "";
$dbname = "licenta";

$conn = new mysqli($servername, $username, $password, $dbname);

if ($conn->connect_error) {
  die(json_encode(['status' => 'error', 'message' => 'Connection failed: ' . $conn->connect_error]));
}

if($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['name']) && isset($_POST['phone']) && isset($_POST['email']) && isset($_POST['animal']) && isset($_POST['date']) && isset($_POST['time'])){
    $name = $_POST['name'];
    $phone = $_POST['phone'];
    $email = $_POST['email'];
    $animal = $_POST['animal'] == 'altele' ? $_POST['otherAnimal'] : $_POST['animal'];
    $date = $_POST['date'];
    $time = $_POST['time'];
    $description = $_POST['description'];

    $sql = "INSERT INTO bookings (nume, telefon, email, tip_animal, data_programare, ora_programare, descriere) VALUES (?, ?, ?, ?, ?, ?, ?)";

    if ($stmt = $conn->prepare($sql)) {
        $stmt->bind_param("sssssss", $name, $phone, $email, $animal, $date, $time, $description);
        if ($stmt->execute() === TRUE) {
            echo "Programare efectuata cu succes";
        } else {
            echo "Eroare: " . $stmt->error;
        }
    }
    $stmt->close();
}

$conn->close();

?>
