<?php
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "licenta";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);

// Check connection
if ($conn->connect_error) {
  die("Connection failed: " . $conn->connect_error);
}

if(isset($_POST['user-id'])) {
  $user_id = $_POST['user-id'];

  $sql = "DELETE FROM utilizatori WHERE id=$user_id";
  
  if ($conn->query($sql) === TRUE) {
    echo "Utilizatorul a fost șters cu succes.";
  } else {
    echo "Eroare la ștergerea utilizatorului: " . $conn->error;
  }

  $conn->close();
} else {
  echo "No user-id provided";
}
?>
