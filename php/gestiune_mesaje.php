<?php
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "licenta";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);

// Check connection
if ($conn->connect_error) {
  die("Connection failed: " . $conn->connect_error);
}

if (isset($_POST['message-id'])) {
  $message_id = $_POST['message-id'];

  $sql = "DELETE FROM mesaje WHERE id=$message_id";
  if ($conn->query($sql) === TRUE) {
    echo "Mesaj sters cu succes!:)";
  } else {
    echo "Error deleting record: " . $conn->error;
  }
}

$conn->close();
?>
