<?php
session_start();

echo json_encode(array(
  "is_logged_in" => isset($_SESSION["isLogged"]) && $_SESSION["isLogged"] == true,
));
