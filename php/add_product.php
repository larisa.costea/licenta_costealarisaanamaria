<?php
// Datele serverului și baza de date
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "licenta";

// Crearea unei noi conexiuni
$conn = new mysqli($servername, $username, $password, $dbname);

// Verificarea conexiunii
if ($conn->connect_error) {
    die("Conexiune eșuată: " . $conn->connect_error);
}

// ...

if ($_SERVER["REQUEST_METHOD"] === "POST") {
    // Verificați dacă formularul este trimis

    // Extrageți datele produsului din formular
    $product_name = $_POST['product-name'];
    $product_description = $_POST['product-description'];
    $product_price = $_POST['product-price'];
    $product_category = $_POST['product-category'];
    $product_quantity = $_POST['product-quantity'];
    $imagePath = $_POST['product-image']; // Preiați URL-ul imaginii din formular

    // Introduceți produsul în baza de date cu un statement pregătit
    $stmt = $conn->prepare("INSERT INTO produse (ProductName, Description, Price, Category, Quantity, ImagePath) VALUES (?, ?, ?, ?, ?, ?)");
    $stmt->bind_param("ssdsis", $product_name, $product_description, $product_price, $product_category, $product_quantity, $imagePath);

    if ($stmt->execute() === TRUE) {
        echo "Produs adăugat cu succes";
    } else {
        echo "Eroare: " . $stmt->error;
    }

    $stmt->close();
} else {
    echo "Eroare MySQL: " . $conn->error;
}

$conn->close();
?>
