<?php
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "licenta";

$conn = new mysqli($servername, $username, $password, $dbname);

if ($conn->connect_error) {
  die("Connection failed: " . $conn->connect_error);
}

if (isset($_POST['booking-id'])) {
  $booking_id = $_POST['booking-id'];

  $sql = "DELETE FROM bookings WHERE id=$booking_id";
  if ($conn->query($sql) === TRUE) {
    echo "Programare stearsa cu succes!";
  } else {
    echo "Error deleting record: " . $conn->error;
  }
}

$conn->close();
?>
