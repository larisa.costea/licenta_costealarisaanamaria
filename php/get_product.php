<?php
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "licenta";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);

// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

$product_id = $_POST['id'];

$sql = "SELECT * FROM produse WHERE ProductID=$product_id";
$result = $conn->query($sql);

if ($result->num_rows > 0) {
    $row = $result->fetch_assoc();

    // Returnați datele produsului în format JSON
    echo json_encode($row);
} else {
    echo "Product not found";
}

$conn->close();
?>
