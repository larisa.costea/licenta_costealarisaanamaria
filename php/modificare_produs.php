<?php
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "licenta";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);

// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

$product_id = $_POST['product-id'];
$product_name = $_POST['product-name'];
$product_price = $_POST['product-price'];
$product_category = $_POST['product-category'];
$product_quantity = $_POST['product-quantity'];
$product_description = $_POST['product-description'];


// Actualizați produsul în baza de date
$sql = "UPDATE produse SET ProductName='$product_name', Price=$product_price, Category='$product_category', Quantity=$product_quantity, Description='$product_description' WHERE ProductID=$product_id";

if ($conn->query($sql) === TRUE) {
    echo "Produsul a fost actualizat cu succes";
} else {
    echo "Eroare: " . $conn->error;
}

$conn->close();
?>
