<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Coșul de cumpărături</title>
  <!-- Bootstrap CSS link -->
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
</head>

<body>
  <div class="container">
    <h1>Coșul de cumpărături</h1>
    <div class="row">
      <div class="col-md-8">
        <table class="table">
          <thead>
            <tr>
              <th>Produs</th>
              <th>Cantitate</th>
              <th>Preț</th>
              <th>Total</th>
            </tr>
          </thead>
          <tbody id="cart-items">
            <!-- Aici vor fi afișate produsele din coș -->
          </tbody>
        </table>
      </div>
    </div>
  </div>

  <!-- Bootstrap JS link -->
  <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script>
  <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.5.3/dist/umd/popper.min.js"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>

  <script>
    // Verifică dacă există produse în coș
    if (sessionStorage.getItem('cart')) {
      // Obține produsele din coș sub forma unui obiect JSON
      var cartItems = JSON.parse(sessionStorage.getItem('cart'));

      // Selectează elementul HTML pentru afișarea produselor din coș
      var cartItemsContainer = document.getElementById('cart-items');

      // Parcurge fiecare produs din coș și adaugă-l în tabelul HTML
      for (var productId in cartItems) {
        var quantity = cartItems[productId];
        // Aici poți crea rândurile tabelului pentru fiecare produs (nume, preț, cantitate etc.)
        var row = '<tr><td>' + productId + '</td><td>' + quantity + '</td><td>Preț produs</td><td>Total</td></tr>';
        cartItemsContainer.innerHTML += row;
      }
    } else {
      // Coșul de cumpărături este gol
      var cartItemsContainer = document.getElementById('cart-items');
      cartItemsContainer.innerHTML = '<tr><td colspan="4">Coșul de cumpărături este gol.</td></tr>';
    }
  </script>
</body>

</html>
