<?php
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "licenta";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);

// Check connection
if ($conn->connect_error) {
  die("Connection failed: " . $conn->connect_error);
}

$product_id = $_POST['product-id'];

$sql = "DELETE FROM produse WHERE ProductID=$product_id";

if ($conn->query($sql) === TRUE) {
  echo "Product deleted successfully";
} else {
  echo "Error: " . $sql . "<br>" . $conn->error;
}

$conn->close();
?>
