<?php
session_start();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Happy Paws</title>
    <!-- font awesome link-->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css">
    <!-- swiper css link -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/swiper@9/swiper-bundle.min.css" />
    <!-- custom css link -->
    <link rel="stylesheet" href="style.css">
   
   
    


</head>
<body>
    <header class="header">
        <a href="index.php" class="logo"> <i class="fa-solid fa-paw"></i>Happy Paws</a>
        <nav class="navbar">
            <a class="active" href="#Acasa">Acasa</a>
            <a href="about.php">Despre Noi</a>
            <a href="preturi.php">Servicii/Tarife</a>
            <a href="cont.php">Contul Meu</a>
            <a href="contactCOD.php">Contact</a>
            <a href="blog.php">Blog</a>
            <a href="test.php">Produse</a>
            <?php
if (isset($_SESSION["isAdmin"]) && $_SESSION["isAdmin"] == true) {
    echo '<a href="admin.php">Panou Admin</a>';
}
?>
        </nav>
        <div class="icons">
            <div id="login-btn" class="fas fa-user"></div>
            <div id="menu-btn" class="fas fa-bars"></div>
            
        </div>
        <!-- login form-->
        <?php
if (isset($_SESSION["isLogged"]) && $_SESSION["isLogged"] == true) {
    // Utilizatorul este autentificat
    echo '<a href="logout.php" class="btn btn-logout">Logout</a>';
} else {
    // Utilizatorul nu este autentificat
?>
        <form action="login.php" method="POST" class="login-form">
            <h3>login form <i class="fa-solid fa-paw"></i></h3>
            <input type="email" name="email" placeholder="Introduceti adresa de email" class="box">
            <input type="password" name="parola" placeholder="Introduceti parola" class="box">
            <label for="rol">Selectează rolul:</label>
            <select name="rol" id="rol">
                <option value="user">Utilizator</option>
                <option value="admin">Admin</option>
            </select>
            <button type="submit" class="btn">login</button>
        </form>
        <div id="message-container"></div>
        <?php
}
?>
    </header>
<!--header section ends -->

<!--home section starts-->
<section class="home" id="home">
    <div class="content" >
        <h3> Sănătate <br><span> pentru <br> prietenul tău</span> </h3>
        <p>Cabinetul veterinar Happy Paws</p>
        <a href="contactCOD.php" class="btn">Programare</a>
    </div>
    
    <div class="custom-shape-divider-bottom-1684086027">
        <svg data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1200 120" preserveAspectRatio="none">
            <path d="M321.39,56.44c58-10.79,114.16-30.13,172-41.86,82.39-16.72,168.19-17.73,250.45-.39C823.78,31,906.67,72,985.66,92.83c70.05,18.48,146.53,26.09,214.34,3V0H0V27.35A600.21,600.21,0,0,0,321.39,56.44Z" class="shape-fill"></path>
        </svg>
    </div>
     </section>
<!-- home section ends-->
 <!-- about -->

 <section class="about" id="about">

    <h2 class="deco-title">Servicii și tarife</h2>

    <div class="box-container">

        <div class="image">
            <img src="pisic.png" alt="">
        </div>

        <div class="content">
            <h3 class="title">Servicii medical veterinare complete</h3>
            <p>Bine ai venit la cabinetul veterinar HappyPaws, unde îngrijirea și 
                sănătatea animalelor sunt prioritare. Suntem dedicați în
                 oferirea celor mai bune servicii medicale pentru animalele de companie,
                  pentru a le ajuta să ducă o viață sănătoasă și fericită.</p>
            
            <div class="icons-container">
                <div class="icons">
                    <h3><strong>O echipa de medici veterinari profesionisti</strong><i class="fas fa-heart-pulse"></i> </h3>
                    
                </div>
                <div class="icons">
                    <i class="fas fa-check"></i>
                    <h3><strong>Servicii medical veterinare de top</strong></h3>
                </div>
                <div class="icons">
                    <i class="fas fa-dog"></i>
                    <h3><strong>Magazin si farmacie veterinara</strong></h3>
                </div>
                <a href="preturi.php" class="btn">Servicii/Tarife</a>
            </div>
        </div>

    

</section>


<!-- end -->
<!--Description section starts-->
<div class="description">
<div class="box-container">
    <div class="content">
        <h3>Cabinetul Veterinar Happy Paws</h3>
        <p>HappyPaws a fost fondat în 2005 de Dr. John Smith, cu pasiunea de a oferi 
            îngrijire și afecțiune animalelor de companie.
             De atunci, am crescut și am devenit un cabinet veterinar de încredere,
              cu o echipă dedicată și servicii de înaltă calitate.</p>
            
              <a href="about.php" class="btn">Despre Noi</a>
    </div>
    
    <div class="image">
        <img src="pat.png" alt="">
    </div>
    
</div>
    
</div>
<!--Description section ends-->
<!-- services section starts  -->

<section class="services" id="services">

    <h1 class="heading"> <span>Serviciile</span> noastre</h1>
   

    <div class="box-container">

        <div class="box">
            <i class="fas fa-dog"></i>
            <h3>Testare Rapida Boli Infectioase/Parazitare</h3>
            <a href="preturi.php" class="btn">Detalii..</a>
        </div>

        <div class="box">
            <i class="fas fa-cat"></i>
            <h3> Imunizare</h3>
            <a href="preturi.php" class="btn">Detalii..</a>
        </div>

        <div class="box">
            <i class="fas fa-bath"></i>
            <h3>Îngrijire</h3>
            <a href="preturi.php" class="btn">Detalii..</a>
        </div>

        <div class="box">
            <i class="fas fa-drumstick-bite"></i>
            <h3>Sfaturi nutritionale</h3>
            <a href="preturi.php" class="btn">Detalii..</a>
        </div>

        <div class="box">
            <i class="fas fa-baseball-ball"></i>
            <h3>Deparazitari</h3>
            <a href="preturi.php" class="btn">Detalii..</a>
        </div>

        <div class="box">
            <i class="fas fa-heartbeat"></i>
            <h3>Interventii Chirurgicale</h3>
            <a href="preturi.php" class="btn">Detalii..</a>
        </div>

    </div>

</section>

<!-- services section ends -->
<section class="contact" id="contact">

    <div class="image">
        <img src="hero.png" alt="">
    </div>

    <form action="contact.php" method="POST">
        <h3>contact us</h3>
        <input type="text" name="name" placeholder="Numele dvs." class="box">
        <input type="email" name="email" placeholder="Email.." class="box">
        <input type="tumber" name="phone" placeholder="Nr. de telefon.." class="box">
        <textarea name="message" placeholder="Mesajul dvs." id="" cols="30" rows="10"></textarea>
        <input type="submit" value="send message" class="btn">
    </form>

<!-- New chatbot -->
<div id="chatbot-icon" class="chatbot-icon">
        <i class="fas fa-comments"></i>
    </div>


<!-- Chatbot container -->
<div class="chatbot-container">
    <div class="chatbot-content"></div>
    <div class="chatbot-input">
        <textarea id="chat-input" placeholder="Introduceți întrebarea dvs."></textarea>
        <span id="send-btn">Trimite</span>
    </div>
    <div class="close-btn">X</div>
</div>






<!-- Chatbot JavaScript --> 
   
  

</section>


<footer class="footer">
  
      <div class="footer-info">
        
        <h2>Happy Paws</h2>
        <p>Ne dedicăm să oferim cele mai bune servicii pentru a vă ajuta să vă îngrijiți de animalele dumneavoastră de companie. Ne pasă de bunăstarea lor și vrem să vă asigurăm că sunt în cele mai bune mâini.</p>
      </div>
      <div class="footer-links">
        <h3>Link-uri utile</h3>
        <ul>
          <li><a href="index.html">Acasă</a></li>
          <li><a href="about.html">Despre noi</a></li>
          <li><a href="preturi.html">Servicii/Tarife</a></li>
          <li><a href="cont.html">Contul meu</a></li>
          <li><a href="contact.html">Contact</a></li>
        </ul>
      </div>
      <div class="footer-social">
        <h3>Urmăriți-ne</h3>
        <ul>
          <li><a href="#"><i class="fab fa-facebook"></i> Facebook</a></li>
          <li><a href="#"><i class="fab fa-instagram"></i> Instagram</a></li>
          <li><a href="#"><i class="fab fa-twitter"></i> Twitter</a></li>
          <li><a href="#"><i class="fab fa-linkedin"></i> LinkedIn</a></li>
        </ul>
      </div>
      <div class="footer-contact">
        <h3>Contact</h3>
        <p><i class="fa-solid fa-envelope"></i> contact@happypaws.com</p>
        <p><i class="fa-solid fa-phone"></i> +123456789</p>
        <p><i class="fa-solid fa-map-marker-alt"></i> Str. Exemplu, Nr. 1, Oras, Judet, Romania</p>
      </div>
    </div>
    <div class="footer-bottom">
      <p>&copy; 2023 Happy Paws. Toate drepturile rezervate.</p>
    </div>
  </footer>
  
 
 <!-- jquery cdn link -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.4/jquery.min.js"></script>
    <!-- swiper js link -->
    <script src="https://cdn.jsdelivr.net/npm/swiper@9/swiper-bundle.min.js"></script>
    <!-- custom js link -->
    <script src="script.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="chatbot.js"></script>
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    


<script>

$(document).ready(function() {
  $(".login-form").on('submit', function(e) {
    e.preventDefault();

    var form = $(this);
    var formData = form.serialize();

    $.ajax({
      type: "POST",
      url: "login.php",
      data: formData,
      success: function(response) {
        alert( response);
        location.reload(); // Reîncarcă pagina curentă
      },
      error: function() {
        alert('A apărut o eroare în timpul autentificării.');
      }
    });
  });
});



</script>

</body>
</html>

