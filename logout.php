<?php
session_start();

// Distrugerea sesiunii
session_unset();
session_destroy();

// Redirecționare către pagina de login
header("Location: index.php");
exit();
?>
