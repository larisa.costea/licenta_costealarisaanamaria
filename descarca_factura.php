<?php
if (isset($_GET['fisier'])) {
    $numeFisier = $_GET['fisier'];

    header('Content-Type: application/octet-stream');
    header('Content-Disposition: attachment; filename="' . $numeFisier . '"');
    header('Content-Length: ' . filesize($numeFisier));

    readfile($numeFisier);
    exit();
} else {
    echo 'Eroare: Nume fișier lipsă.';
}
?>
