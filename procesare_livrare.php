<?php
session_start();

// Verificăm dacă există variabila listCards în sesiune
if (isset($_SESSION['listCards'])) {
  $listCards = $_SESSION['listCards'];
} else {
  $listCards = null;
}

// Procesarea datelor de livrare
if ($_SERVER["REQUEST_METHOD"] === "POST") {
  // Preiați datele din formular
  $nume = $_POST["nume"];
  $prenume = $_POST["prenume"];
  $adresa = $_POST["adresa"];
  $oras = $_POST["oras"];
  $cod_postal = $_POST["cod_postal"];
  $telefon = $_POST["telefon"];
  $email = $_POST["email"];
  $metoda_plata = $_POST["metoda_plata"];
  $total_plata = $_POST["total_plata"];
  $produse_cumparate= $_POST["produse_cumparate"];

  // Efectuați acțiunile dorite cu datele primite, cum ar fi inserarea în baza de date, trimiterea de email-uri, etc.

  // Conectați-vă la baza de date și verificați conexiunea
  $conn = mysqli_connect("localhost", "root", "", "licenta");
  if (!$conn) {
    die("Conexiunea la baza de date a eșuat: " . mysqli_connect_error());
  }

  // Construiți și executați interogarea SQL pentru inserarea datelor
  $sql = "INSERT INTO livrari (nume, prenume, adresa, oras, cod_postal, telefon, email, metoda_plata, total_plata, data_livrare,produse_cumparate) 
          VALUES ('$nume', '$prenume', '$adresa', '$oras', '$cod_postal', '$telefon', '$email', '$metoda_plata', '$total_plata', NOW(),'$produse_cumparate')";

  if (mysqli_query($conn, $sql)) {
    // Datele de livrare au fost inserate cu succes în baza de date
    // Setăm variabila de sesiune pentru a indica finalizarea comenzii
    $_SESSION["comanda_finalizata"] = true;

    // Setăm ID-ul livrării în sesiune
    $_SESSION['id_livrare'] = mysqli_insert_id($conn);

    echo '<script>
    window.onload = function() {
      alert("' . $_SESSION['confirm_message'] . '");
    }
  </script>';
  } else {
    // A apărut o eroare la inserarea datelor de livrare în baza de date
    mysqli_close($conn); // Închidem conexiunea la baza de date
    die("Eroare la inserarea datelor de livrare: " . mysqli_error($conn));
  }

  // Închideți conexiunea la baza de date
  mysqli_close($conn);

  // Redirecționează utilizatorul înapoi la pagina de livrare
  header("Location: pagina_livrare.php");
  exit();
}
?>
