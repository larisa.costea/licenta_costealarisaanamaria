<?php
session_start();
// Verifică dacă s-a trimis ID-ul livrării prin metoda POST
if (isset($_POST['id_livrare'])) {
    $idLivrare = $_POST['id_livrare'];

    // Conectare la baza de date
    $servername = "localhost";
    $username = "root";
    $password = "";
    $dbname = "licenta";
    $conn = new mysqli($servername, $username, $password, $dbname);
    if ($conn->connect_error) {
        die("Conexiunea la baza de date a eșuat: " . $conn->connect_error);
    }

   // Obține datele livrării și produsele asociate din baza de date
   $query = "SELECT * FROM livrari WHERE id = $idLivrare";
   $result = $conn->query($query);
   if ($result === false) {
       die("Eroare la interogarea bazei de date: " . $conn->error);
   }
   $livrare = $result->fetch_assoc();
   if ($livrare === null) {
       die("Nu există date de returnat.");
   }

$query = "SELECT * FROM comenzi WHERE id_comanda = $idLivrare";
$result = $conn->query($query);
if ($result === false) {
    die("Eroare la interogarea bazei de date: " . $conn->error);
}
$comenzi = array();
while ($row = $result->fetch_assoc()) {
    $comenzi[] = $row;
}


    // Generare factură HTML
    $html = '<!DOCTYPE html>
    <html>
    <head>
        <meta charset="UTF-8">
        <title>Factură</title>
        <style>
            body {
                font-family: Arial, sans-serif;
                margin: 0;
                padding: 0;
                background-color: #f0f0f0;
            }

            .container {
                width: 80%;
                margin: 0 auto;
                background-color: #fff;
                padding: 20px;
                border-radius: 5px;
                box-shadow: 0px 0px 10px 0px rgba(0,0,0,0.1);
                
            }

            .header {
                display: flex;
                justify-content: space-between;
                align-items: center;
                border-bottom: 1px solid #000;
                padding-bottom: 20px;
                margin-bottom: 20px;
            }

            .header h1 {
                margin: 0;
                text-align: center;
                flex-grow: 2;
            }

            .details, .total {
                display: flex;
                justify-content: space-between;
                margin-bottom: 20px;
            }

            .details div, .total div {
                width: 45%;
            }

            .products {
                margin-bottom: 20px;
            }

            h2, p {
                margin-bottom: 15px;
            }

            p {
                line-height: 1.6;
            }

        </style>
    </head>
    <body>
        <div class="container">
            <div class="header">
                <div>
                    <p>Număr factura: ' . $idLivrare . '</p>
                    <p>Data emitere: ' . date("d-m-Y") . '</p>
                </div>
                <h1>Factură</h1>
            </div>
            <div class="details">
                <div>
                    <h2>Detalii livrare</h2>
                    <p>Nume: ' . $livrare['nume'] . '</p>
                    <p>Prenume: ' . $livrare['prenume'] . '</p>
                    <p>Adresă: ' . $livrare['adresa'] . '</p>
                    <p>Oraș: ' . $livrare['oras'] . '</p>
                    <p>Cod Poștal: ' . $livrare['cod_postal'] . '</p>
                </div>
                <div>
                    <h2>Contact</h2>
                    <p>Telefon: ' . $livrare['telefon'] . '</p>
                    <p>Email: ' . $livrare['email'] . '</p>
                </div>
            </div>
            <div class="products">
                <h2>Produse comandate</h2>
                <p>' . nl2br($livrare['produse_cumparate']) . '</p>
            </div>
            <div class="total">
                <div>
                    <h2>Metoda de plată</h2>
                    <p>' . $livrare['metoda_plata'] . '</p>
                </div>
                <div>
                    <h2>Total plată</h2>
                    <p>' . $livrare['total_plata'] . '</p>
                </div>
            </div>
        </div>
    </body>
    </html>';

    $sql = "SELECT MAX(id) AS max_id FROM livrari";
$result = $conn->query($sql);
if ($result->num_rows > 0) {
    $row = $result->fetch_assoc();
    $idLivrare = $row['max_id'];
    // Restul codului tau care foloseste $idLivrare
} else {
    echo 'Nu a fost gasita nicio livrare.';
}

    
    // Salvează factura într-un fișier
    $numeFisier = 'factura_' . $idLivrare . '.html';
    file_put_contents($numeFisier, $html);

    // Redirecționează către pagina de descărcare a facturii
    header('Location: descarca_factura.php?fisier=' . $numeFisier);
    exit();
} else {
    echo 'Eroare: ID livrare lipsă.';
}
?>
